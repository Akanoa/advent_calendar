fun runPart1(lines: List<String>) : Int {

    return 0
}

fun part1() {
    val lines = readInputFile("day08/src/main/resources/input.txt")

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")
}