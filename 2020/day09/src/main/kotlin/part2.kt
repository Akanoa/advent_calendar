import java.lang.Error

fun runPart2(lines: List<String>, preambleSize: Int) : Long {

    val data = lines.map { line -> line.toLong() }
    val invalid = findInvalid(data, preambleSize)
    val reverseChunk = data.subList(0, invalid.first).reversed()

    for(pointerSize in 2 until reverseChunk.size) {


        for (pointerStart in 0 until reverseChunk.size - pointerSize + 1) {
            val chunk = reverseChunk.subList(pointerStart, pointerSize + pointerStart)
            if (chunk.sum() == invalid.second) {
                return chunk.maxOrNull()?.plus(chunk.minOrNull()!!) ?: throw Error("Something is wrong")
            }
        }
    }

    return 0
}

fun part2() {
    val lines = readInputFile("day09/src/main/resources/input.txt")

    val result = runPart2(lines, 25)
    println("Le résultat part 2 est $result")
}