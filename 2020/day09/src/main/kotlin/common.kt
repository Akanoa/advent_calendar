import com.github.shiguruikai.combinatoricskt.permutations

fun getAllowedNumbers(preamble: List<Long>) : List<Long> {

    return preamble.permutations(2)
        .toList()
        .map { permutation -> permutation.sum() }
}


fun findInvalid(data: List<Long>, preambleSize: Int) : Pair<Int, Long> {

    var pointer = preambleSize

    while (true) {

        if (pointer > data.size - 1) {
            break
        }

        val allowed = getAllowedNumbers(data.subList(pointer - preambleSize, pointer))

        if(!allowed.contains(data[pointer])) {
            return Pair(pointer, data[pointer])
        }
        pointer++

    }

    return Pair(0, 0)
}