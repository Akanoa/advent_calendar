fun runPart1(lines: List<String>, preambleSize: Int) : Long {

    val data = lines.map { line -> line.toLong() }

    return findInvalid(data, preambleSize).second
}

fun part1() {
    val lines = readInputFile("day09/src/main/resources/input.txt")

    val result = runPart1(lines, 25)
    println("Le résultat part 1 est $result")
}