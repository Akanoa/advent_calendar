package test

import getResourceAsText
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart1

internal class Part1KtTest {

    @Test
    fun `should run part 1`() {
        val lines = getResourceAsText("/simple.txt")
        assertEquals(127, runPart1(lines, 5))
    }
}