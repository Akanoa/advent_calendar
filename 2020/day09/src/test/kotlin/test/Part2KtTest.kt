package test

import getResourceAsText
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart2

internal class Part2KtTest {

    @Test
    fun `should run part 2`() {
        val lines = getResourceAsText("/simple.txt")
        assertEquals(62, runPart2(lines, 5))
    }
}