import java.lang.Error

sealed class Height {

    data class Inches(val size: Int) : Height()
    data class Centimeters(val size: Int) : Height()
    data class Raw(val hgt: String) : Height() {

        fun formatHeight() : Height {

            val regex = Regex("(\\d+)(in|cm)")

            val match = regex.find(this.hgt)

            val size = match?.groupValues?.get(1)?.toInt()

            if (match != null) {
                return when(match.groupValues[2]) {
                    "in" -> size?.let { Inches(it) }
                    "cm" -> size?.let { Centimeters(it) }
                    else -> null
                }?: throw Error("Bad height kind")
            }

            throw Error("Bad height kind2")
        }
    }

    fun isValid() : Boolean {

        if (this !is Raw) {
            return false
        }

        try {

            return when(val eyeColor = this.formatHeight()) {
                is Inches -> eyeColor.size in 59..76
                is Centimeters -> eyeColor.size in 150..193
                else -> return false
            }

        } catch (error: Error) {
            return false
        }

    }
}

enum class EyeColorEnum {
    Amber,
    Blue,
    Brown,
    Grey,
    Green,
    Hazel,
    Other
}

sealed class EyeColor {
    data class Color(val color: EyeColorEnum) : EyeColor()
    data class Raw(var eyeColor: String) : EyeColor() {

        fun format() : EyeColor  {

            return when(this.eyeColor){

                "amb" -> EyeColorEnum.Amber
                "blu" -> EyeColorEnum.Blue
                "brn" -> EyeColorEnum.Brown
                "gry" -> EyeColorEnum.Grey
                "grn" -> EyeColorEnum.Green
                "hzl" -> EyeColorEnum.Hazel
                "oth" -> EyeColorEnum.Other
                else -> null

            }?.let { color -> Color(color) } ?: throw Error("Invalid eye color")
        }

    }

    fun formatEyeColor() : EyeColor {
        return when(this) {
            is Raw -> this.format()
            else -> this
        }
    }

    fun isValid() : Boolean {

        val validEyeColor = listOf(
            "amb", "blu", "brn", "gry", "grn", "hzl", "oth"
        )

        if (this is Raw) {
            return validEyeColor.contains(this.eyeColor)
        }
        return true
    }
}

data class Passport(
    val byr: Int?,
    val iyr: Int?,
    val eyr: Int?,
    var hgt: Height?,
    val hcl: String?,
    var ecl: EyeColor?,
    val pid: String?,
    val cid: Int?
) {

    constructor(hgt: String) : this(null, null, null, Height.Raw(hgt), null, null, null, null)
    constructor(hgt: String, ecl: String) : this(null, null, null, Height.Raw(hgt), null, EyeColor.Raw(ecl), null, null)
    constructor(hgt: String?, ecl: String?, hcl: String?) : this(null, null, null, Height.Raw(hgt?:""), hcl, EyeColor.Raw(ecl?:""), null, null)
    constructor(hgt: String?, ecl: String?, hcl: String?, byr: Int?) : this(byr, null, null, Height.Raw(hgt?:""), hcl, EyeColor.Raw(ecl?:""), null, null)
    constructor(hgt: String?, ecl: String?, hcl: String?, byr: Int?, pid: String?) : this(byr, null, null, Height.Raw(hgt?:""), hcl, EyeColor.Raw(ecl?:""), pid, null)

    fun isValid() : Boolean {

        if (
            this.byr == null ||
            this.iyr == null ||
            this.eyr == null ||
            this.hgt == null ||
            this.hcl == null ||
            this.ecl == null ||
            this.pid == null
        ){
            return false
        }

        return true
    }

}

open class PassportWrapper(private val passport: Passport) {

    fun isValid(): Boolean {

        if (!passport.isValid()) {
            return false
        }

        if (
            !this.isValidBirthday() ||
            !this.isValidIssueYear() ||
            !this.isValidExpirationYear() ||
            !this.isValidHeight() ||
            !this.isValidEyeColor() ||
            !this.isValidHairColor() ||
            !this.isValidPid()
        ) {
            return false
        }

        return true
    }

    fun isValidBirthday(): Boolean {
        return this.passport.byr in 1920..2002
    }

    private fun isValidIssueYear(): Boolean {
        return this.passport.iyr in 2010..2020
    }

    private fun isValidExpirationYear(): Boolean {
        return this.passport.eyr in 2020..2030
    }

    fun isValidPid(): Boolean {
        val regex = Regex("^[0-9]{9}$")
        return this.passport.pid?.let { regex.find(it) } != null
    }

    fun isValidHairColor(): Boolean {
        val regex = Regex("^#[a-f0-9]{6}$")
        return this.passport.hcl?.let { regex.find(it) } != null
    }

    fun isValidEyeColor(): Boolean {
        return this.passport.ecl?.isValid()!!
    }

    fun isValidHeight(): Boolean {
        return this.passport.hgt?.isValid()!!
    }
}

fun getFields(passportLine: String) : Passport {

    val regex = Regex("(:?((?<key>[a-z]{3}):(?<value>[#a-z0-9]+))[\\s\\r\\n]?)")

    val hashMap = HashMap<String, String>()

    regex.findAll(passportLine)
        .map { match -> match.groupValues.subList(3, 5)}
        .forEach { (key, value) -> hashMap[key] = value }

    return Passport(
        hashMap["byr"]?.toIntOrNull(),
        hashMap["iyr"]?.toIntOrNull(),
        hashMap["eyr"]?.toIntOrNull(),
        hashMap["hgt"]?.let { Height.Raw(it) },
        hashMap["hcl"],
        hashMap["ecl"]?.let { EyeColor.Raw(it) },
        hashMap["pid"],
        hashMap["cid"]?.toIntOrNull());
}

fun getPassports(lines: List<String>) : List<Passport> {

    return lines
        .joinToString("") { line -> line + "\n" }
        .split("\n\n")
        .map { line -> getFields(line) }
}