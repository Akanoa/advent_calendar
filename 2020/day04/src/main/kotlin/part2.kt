fun runPart2(lines: List<String>) : Int {

    return getPassports(lines)
        .map { passport ->  PassportWrapper(passport) }
        .filter { passport -> passport.isValid() }
        .count()
}


fun part2() {
    val lines = readInputFile("day04/src/main/resources/input.txt")

    val result = runPart2(lines)
    println("Le résultat part 2 est $result")
}