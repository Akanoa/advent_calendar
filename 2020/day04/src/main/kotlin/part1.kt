fun runPart1(lines: List<String>) : Int {

    return getPassports(lines)
        .filter { passport -> passport.isValid() }
        .count()
}

fun part1() {
    val lines = readInputFile("day04/src/main/resources/input.txt")

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")
}