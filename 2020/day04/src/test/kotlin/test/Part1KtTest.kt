package test

import Passport
import getFields
import getPassports
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart1

internal class Part1KtTest {

    @Test
    fun `should get fields from string`() {
        val data = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\n" +
                "byr:1937 iyr:2017 cid:147 hgt:183cm"
        val expected = Passport(1937, 2017, 2020, Height.Raw("183cm"), "#fffffd", EyeColor.Raw("gry"), "860033327", 147)
        val result = getFields(data)
        assertEquals(expected, result)

        val data2 = "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n" +
                "hcl:#cfa07d byr:1929"
        val expected2 = Passport(1929, 2013, 2023, null, "#cfa07d", EyeColor.Raw("amb"), "028048884", 350)
        val result2 = getFields(data2)
        assertEquals(expected2, result2)

        val data3 = "hcl:#ae17e1 iyr:2013\n" +
                "eyr:2024\n" +
                "ecl:brn pid:760753108 byr:1931\n" +
                "hgt:179cm"
        val expected3 = Passport(1931, 2013, 2024, Height.Raw("179cm"), "#ae17e1", EyeColor.Raw("brn"), "760753108", null)
        val result3 = getFields(data3)
        assertEquals(expected3, result3)

        val data4 = "hcl:#cfa07d eyr:2025 pid:166559648\n" +
                "iyr:2011 ecl:brn hgt:59in"
        val expected4 = Passport(null, 2011, 2025, Height.Raw("59in"), "#cfa07d", EyeColor.Raw("brn"), "166559648", null)
        val result4 = getFields(data4)
        assertEquals(expected4, result4)

    }

    @Test
    fun `should return list of passport`() {
        val lines = listOf(
            "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
            "byr:1937 iyr:2017 cid:147 hgt:183cm",
            "",
            "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884",
            "hcl:#cfa07d byr:1929",
            "",
            "hcl:#ae17e1 iyr:2013",
            "eyr:2024",
            "ecl:brn pid:760753108 byr:1931",
            "hgt:179cm",
            "",
            "hcl:#cfa07d eyr:2025 pid:166559648",
            "iyr:2011 ecl:brn hgt:59in"
        )
        val expected = listOf(
            Passport(1937, 2017, 2020, Height.Raw("183cm"), "#fffffd", EyeColor.Raw("gry"), "860033327", 147),
            Passport(1929, 2013, 2023, null, "#cfa07d", EyeColor.Raw("amb"), "028048884", 350),
            Passport(1931, 2013, 2024, Height.Raw("179cm"), "#ae17e1", EyeColor.Raw("brn"), "760753108", null),
            Passport(null, 2011, 2025, Height.Raw("59in"), "#cfa07d", EyeColor.Raw("brn"), "166559648", null)
        )
        val result =  getPassports(lines)
        assertEquals(expected, result)
    }

    @Test
    fun `should run part 1`() {
        val lines = listOf(
            "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
            "byr:1937 iyr:2017 cid:147 hgt:183cm",
            "",
            "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884",
            "hcl:#cfa07d byr:1929",
            "",
            "hcl:#ae17e1 iyr:2013",
            "eyr:2024",
            "ecl:brn pid:760753108 byr:1931",
            "hgt:179cm",
            "",
            "hcl:#cfa07d eyr:2025 pid:166559648",
            "iyr:2011 ecl:brn hgt:59in"
        )
        val expected = 2
        val result = runPart1(lines)
        assertEquals(expected, result)

    }
}