package test

import Passport
import PassportWrapper
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart2

internal class Part2KtTest {

    @Test
    fun `should valid height`() {

        val passport1 = Passport("60in")
        val passport2 = Passport("190cm")
        assert(PassportWrapper(passport1).isValidHeight())
        assert(PassportWrapper(passport2).isValidHeight())

        // bad passport
        assert(!PassportWrapper(Passport("190in")).isValidHeight())
        assert(!PassportWrapper(Passport("40in")).isValidHeight())

        assert(!PassportWrapper(Passport("200cm")).isValidHeight())
        assert(!PassportWrapper(Passport("120cm")).isValidHeight())


        assert(!PassportWrapper(Passport("190")).isValidHeight())
    }

    @Test
    fun `should valid birthday`() {
        assert(PassportWrapper(Passport(null, null, null, 2002, )).isValidBirthday())
        assert(!PassportWrapper(Passport(null, null, null, 2003, )).isValidBirthday())
    }

    @Test
    fun `should valid pid`() {
        assert(PassportWrapper(Passport(null, null, null, null,  "000000001")).isValidPid())
        assert(!PassportWrapper(Passport(null, null, null, null,  "0123456789")).isValidPid())
    }

    @Test
    fun `should valid eye color`() {
        val passportAmb = Passport("60in", "amb")
        val passportBlu = Passport("60in", "blu")
        val passportBrn = Passport("60in", "brn")
        val passportGry = Passport("60in", "gry")
        val passportGrn = Passport("60in", "grn")
        val passportHzl = Passport("60in", "hzl")
        val passportOth = Passport("60in", "oth")

        assert(PassportWrapper(passportAmb).isValidEyeColor())
        assert(PassportWrapper(passportBlu).isValidEyeColor())
        assert(PassportWrapper(passportBrn).isValidEyeColor())
        assert(PassportWrapper(passportGry).isValidEyeColor())
        assert(PassportWrapper(passportGrn).isValidEyeColor())
        assert(PassportWrapper(passportHzl).isValidEyeColor())
        assert(PassportWrapper(passportOth).isValidEyeColor())

        assert(!PassportWrapper(Passport("60in", "wat")).isValidEyeColor())

    }

    @Test
    fun `should format eye color`() {
        val passportAmb = Passport("60in", "amb")
        val passportBlu = Passport("60in", "blu")
        val passportBrn = Passport("60in", "brn")
        val passportGry = Passport("60in", "gry")
        val passportGrn = Passport("60in", "grn")
        val passportHzl = Passport("60in", "hzl")
        val passportOth = Passport("60in", "oth")

        assertEquals(EyeColor.Color(EyeColorEnum.Amber), passportAmb.ecl?.let { ecl -> (ecl as EyeColor.Raw).formatEyeColor() })
        assertEquals(EyeColor.Color(EyeColorEnum.Blue), passportBlu.ecl?.let { ecl -> (ecl as EyeColor.Raw).formatEyeColor() })
        assertEquals(EyeColor.Color(EyeColorEnum.Brown), passportBrn.ecl?.let { ecl -> (ecl as EyeColor.Raw).formatEyeColor() })
        assertEquals(EyeColor.Color(EyeColorEnum.Grey), passportGry.ecl?.let { ecl -> (ecl as EyeColor.Raw).formatEyeColor() })
        assertEquals(EyeColor.Color(EyeColorEnum.Green), passportGrn.ecl?.let { ecl -> (ecl as EyeColor.Raw).formatEyeColor() })
        assertEquals(EyeColor.Color(EyeColorEnum.Hazel), passportHzl.ecl?.let { ecl -> (ecl as EyeColor.Raw).formatEyeColor() })
        assertEquals(EyeColor.Color(EyeColorEnum.Other), passportOth.ecl?.let { ecl -> (ecl as EyeColor.Raw).formatEyeColor() })

    }

    @Test
    fun `should check hair color`() {
        assert(PassportWrapper(Passport(null, null, "#123abc")).isValidHairColor())
        assert(!PassportWrapper(Passport(null, null, "#123abz")).isValidHairColor())
        assert(!PassportWrapper(Passport(null, null, "123abc")).isValidHairColor())
    }

    private fun getResourceAsText(path: String): List<String> {
        return object {}.javaClass.getResource(path)
            .readText()
            .split("\n")
    }

    @Test
    fun `should run part 2 valid passports`() {

        val linesValid = getResourceAsText("/valid.txt")
        val resultValid = runPart2(linesValid)
        assertEquals(4, resultValid)

        val linesInvalid = getResourceAsText("/invalid.txt")
        val resultInvalid = runPart2(linesInvalid)
        assertEquals(0, resultInvalid)
    }
}
