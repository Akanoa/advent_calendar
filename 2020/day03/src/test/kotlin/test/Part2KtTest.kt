package test

import Slope
import getNumberOfTrees
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import runPart2

internal class Part2KtTest {

    @Test
    fun `should get number of trees for different slopes`() {
        val lines = listOf(
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#"
        )

        assertEquals(2, getNumberOfTrees(lines, Slope(1, 1)))
        assertEquals(7, getNumberOfTrees(lines, Slope(3, 1)))
        assertEquals(3, getNumberOfTrees(lines, Slope(5, 1)))
        assertEquals(4, getNumberOfTrees(lines, Slope(7, 1)))
        assertEquals(2, getNumberOfTrees(lines, Slope(1, 2)))

    }

    @Test
    fun `should run part 2`() {
        val lines = listOf(
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#"
        )

        val slopes = listOf(
            Slope(1, 1),
            Slope(3, 1),
            Slope(5, 1),
            Slope(7, 1),
            Slope(1, 2),
        )
        val expected : Long = 336

        val result = runPart2(lines, slopes)
        assertEquals(expected, result)
    }
}