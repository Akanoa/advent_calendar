package test

import createRow
import getNext
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import runPart1

internal class Part1KtTest {

    @Test
    fun `should create row from line`() {
        val line = "..##."
        val expected =  listOf(Case.Void, Case.Void, Case.Tree, Case.Tree, Case.Void)
        val result = createRow(line)
        assertEquals(expected, result)
    }

    @Test
    fun `should get next move`() {
        val expected = 1
        val result = getNext(3, 3, 5)
        assertEquals(expected, result)
    }

    @Test
    fun `should run part 1`() {
        val lines = listOf(
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#"
        )
        val expected = 7
        val result = runPart1(lines)
        assertEquals(expected, result)

    }
}