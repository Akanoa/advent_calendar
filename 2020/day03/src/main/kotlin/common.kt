enum class Case {
    Void,
    Tree
}

data class Slope(val right: Int, val down: Int)

fun createRow(line: String) : List<Case> {
    return line
        .split("")
        .subList(1, line.length + 1)
        .map { c -> if (c == "#") Case.Tree else  Case.Void }
}

fun getNext(x: Int, step: Int, length: Int) : Int {
    return (x + step ) % length
}

fun getNumberOfTrees(lines: List<String>, slope: Slope) : Int {

    val linesData = lines.map { line -> createRow(line) }

    var countOfTrees = 0
    var lastX = 0

    for ((i, line) in linesData.subList(1, linesData.size).withIndex()) {

        if (i % slope.down != 0) {
            continue
        }

        lastX = getNext(lastX, slope.right, line.size)
        if (line[lastX] == Case.Tree) {
            countOfTrees++
        }
    }

    return countOfTrees
}
