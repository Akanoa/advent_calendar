fun runPart2(lines: List<String>, slopes: List<Slope>) : Long {

    return slopes
        .fold(1) { acc, slope -> acc * getNumberOfTrees(lines, slope) }
}


fun part2() {
    val lines = readInputFile("day03/src/main/resources/input.txt")

    val slopes = listOf(
        Slope(1, 1),
        Slope(3, 1),
        Slope(5, 1),
        Slope(7, 1),
        Slope(1, 2),
    )

    val result = runPart2(lines, slopes)

    println("Résultat partie#2 $result")
}