fun runPart1(lines: List<String>) : Int {
    return getNumberOfTrees(lines, Slope(3, 1))
}

fun part1() {
    val lines = readInputFile("day03/src/main/resources/input.txt")
    val result = runPart1(lines)

    println("Résultat partie#1 $result")
}
