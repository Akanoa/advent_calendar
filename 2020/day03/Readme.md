# [Jour 3](https://adventofcode.com/2020/day/3)

## Partie 1

### Problème

Le but de ce puzzle est de se déplacer dans une grille de x par y cases.

Cette grille est composée de croix et de rond.

On se déplace à chaque mouvement de 3 vers la droite et de 1 vers le bas. 

![](../assets/images/day03/part1-1.png)

Le but est d'arrivé plus bas que la dernière lignes et de compter les croix rencontré à chaque fin de mouvement.

Cette grille à la particularité d'être un cylindre. Lorsque l'on arrive à l'extrémité droite on se retrouve téléporté à l'extrémité gauche sur la même ligne.

Tout se passait comme si on avait une bande infinie qui se répétait à la droite pour chacune des lignes.

![](../assets/images/day03/part1.png)

### Résolution

Si le monde est un cylindre ça signifie que chaque lignes peut-être représenté sous la forme d'un cercle.

Si on prend la bande:

![](../assets/images/day03/part1-3.png) où la ligne rouge pointillé représente le bord droit.

On peut en faire une représentation circulaire 

![](../assets/images/day03/part1-2.png)

C'est la même chose que sur une horloge, après midi il est 1h de l'après midi.

ici c'est pareil, si on veut se déplacer vers la droite de plus du nombre de mouvement qu'il reste avant d'atteindre la bordure, le reste de ces mouvement se retrouvera retranscrit
à la gauche de la bande.

Cette notion de déplacement circulaire peut être représenté mathématiquement par la notion de modulo.

Donc ici pour résoudre le problème posé étant donné que l'on réalise uniquement un seul mouvement vers le bas et jamais vers le haut.

On va définir 

- une valeur x = 0
- un compteur c = 0
- la largeur l du tableau
- la hauteur h du tableau

On va boucler sur chaque ligne du tableau.

Et appliquer la formule suivante pour trouver le prochain x : `(x + 3) modulo 5`

On vérifie si la position du x actuel de la ligne du dessous est un rond ou une croix.

Si c'est une croix on incrémente le compteur.

Lorsque l'on arrive à la dernière ligne on renvoie le résultat.

### Qu'est ce que j'ai appris

Le concept d'énumération et de data class en Kotlin.

## Partie 2

## Problème

Le problème est exactement le même seul les conditions de mouvement sont différentes.

- Droite 1, Bas 1.
- Droite 3, Bas 1.
- Droite 5, Bas 1.
- Droite 7, Bas 1.
- Droite 1, Bas 2.

Le but de cette partie est de déterminer pour l'ensemble des différents mouvements le nombre de croix rencontré à chaque descente.

Et d'en faire le produit.

### Résolution

La seul chose qui change ici est que si l'on ne descend pas d'une unique ligne à chaque mouvement. On doit passer certaines itération de la boucle.

Pour cela on utilise une fois de plus les modulos. En effet si par exêmple on descend de 2 lignes par mouvements au lieu de 1.

If faut passer une ligne sur deux en revu.

Ce qui nous donne comme condition de "skip" : `{index de ligne} modulo {mouvement vers le bas} != 0`. 

Cette condition est vrai car le modulo représente aussi le reste d'une division euclidienne.

Si on prend l'exemple de passer une ligne sur 3.

| i | i%3 | skip |
|:---:|:-----:|:------:|
| 0 | 0   | faux |
| 1 | 1   | vrai |
| 2 | 2   | vrai |
| 3 | 0   | faux |
| 4 | 1   | vrai |

Seul les lignes 0 et 3 seront traitées.

### Qu'est ce que j'ai appris

Kotlin ne gère pas au runtime l'overflow de mutiplication.

Chaque facteur de la multiplication était un inférieur à l'overflow du type `Int` de Kotlin. Par contre lorsque l'on effectue la multiplication on se retrouve avec un nombre
qui dépasse cette overflow de Int.

Pour la multiplication `90 * 244 * 97 * 92 * 48`:
- avec un retour de fonction en Long : `9406609920`
- avec un retour de fonction en Int : `816675328`

Il faut donc faire très attention à la manipulation des nombre Kotlin car il n'est pas safe en éxécution ou en compilation sur le traitement des opérations mathématique.

Au contraire de Rust. Peut-être un article sur le sujet si j'ai le temps.

## Conclusion

L'idée du puzzle de géométrie circulaire et très cool.

Par contre je reste vexé que les jeux de tests n'est pas permis de détecter le problème de l'overflow en amont 😡

## Puzzle mark

 ⭐⭐⭐☆☆
