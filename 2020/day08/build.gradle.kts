plugins {
    kotlin("jvm") version "1.4.10"
    kotlin("kapt") version "1.4.21"
}

version = "unspecified"

repositories {
    mavenCentral()
    jcenter()
    maven(url="https://dl.bintray.com/arrow-kt/arrow-kt/")
}
val arrow_version = "0.10.4"

dependencies {
//    implementation(kotlin("stdlib"))
    implementation(project(":common"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.0")
    testImplementation ("org.junit.jupiter:junit-jupiter-engine:5.7.0")
    testImplementation ("org.junit.jupiter:junit-jupiter-params:5.7.0")
    implementation("io.arrow-kt:arrow-core:$arrow_version")
    implementation("io.arrow-kt:arrow-syntax:$arrow_version")
}

tasks.withType<Test> {
    useJUnitPlatform()
}
