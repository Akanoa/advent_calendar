import arrow.core.Either

sealed class Operation {
    data class Acc(val arg: Int) : Operation()
    data class Jump(val arg: Int): Operation()
    data class Nop(val arg: Int) : Operation()
}

fun parseLine(line: String) : Operation {

    fun getSign(data: String) : Int {
        return when(data) {
            "+" -> 1
            else -> -1
        }
    }

    val regex = Regex("^(acc|jmp|nop)\\s(\\-|\\+)(\\d+)\$")

    val match = regex.matchEntire(line)?.groupValues?.drop(1) ?: throw Error("Malformed line")
    val operand = getSign(match[1]) * match[2].toInt()

    return when(match[0]) {
        "nop" -> Operation.Nop(operand)
        "acc" -> Operation.Acc(operand)
        "jmp" -> Operation.Jump(operand)
        else -> throw Error("Unknown operand")
    }
}

fun createProgram(lines: List<String>) : List<Operation> {
    return lines.map { line -> parseLine(line) }
}

fun run(program: List<Operation>) : Either<Pair<Int, Int>, Int> {

    var acc = 0
    var pointer = 0
    val indexPointer = hashSetOf<Int>()

    while(true) {

        if (pointer > program.size - 1) {
            break
        }

        if (indexPointer.contains(pointer)) {
            return Either.left(Pair(pointer, acc))
        }

        indexPointer.add(pointer)

        when(val instruction =  program[pointer]) {
            is Operation.Nop -> {
                pointer++
            }
            is Operation.Acc -> {
                pointer++
                acc += instruction.arg
            }
            is Operation.Jump -> pointer += instruction.arg

        }
    }


    return Either.right(acc)
}