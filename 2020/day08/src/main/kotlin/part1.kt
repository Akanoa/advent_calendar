import arrow.core.Either

fun runPart1(lines: List<String>) : Int {

    val program = createProgram(lines)

    return when(val result = run(program)) {
        is Either.Left -> result.a.second
        is Either.Right -> result.b
    }
}

fun part1() {
    val lines = readInputFile("day08/src/main/resources/input.txt")

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")
}