import arrow.core.orNull

fun indexSwappableOperations(program: List<Operation>) : List<Pair<Int, Operation>> {
    return program.foldIndexed(mutableListOf()) { index, acc, operation -> acc.also {
        val newOp = when(operation) {
            is Operation.Nop -> Operation.Jump(operation.arg)
            is Operation.Jump -> Operation.Nop(operation.arg)
            is Operation.Acc -> operation
        }
        if (newOp !is Operation.Acc) {
            acc.add(Pair(index, newOp))
        }
    }}
}


fun runPart2(lines: List<String>) : Int {

    val program = createProgram(lines)
    val swappableOperations = indexSwappableOperations(program)

    for(swap in swappableOperations) {
        val modifiedProgram = program.toMutableList()
        modifiedProgram[swap.first] = swap.second

        val result = run(modifiedProgram)

        if (result.isLeft()) {
            continue
        }

        return result.orNull() ?: throw Error("Something is wrong")

    }

    return 0
}

fun part2() {
    val lines = readInputFile("day08/src/main/resources/input.txt")

    val result = runPart2(lines)
    println("Le résultat part 1 est $result")
}