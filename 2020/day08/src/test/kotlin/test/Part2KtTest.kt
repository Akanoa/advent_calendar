package test

import createProgram
import getResourceAsText
import indexSwappableOperations
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart2

internal class Part2KtTest {

    @Test
    fun `should get swappable operations`() {

        val lines =  getResourceAsText("/simple.txt")
        val program =  createProgram(lines)
        val result = indexSwappableOperations(program)

        val expected = listOf(
            Pair(0, Operation.Jump(0)),
            Pair(2, Operation.Nop(4)),
            Pair(4, Operation.Nop(-3)),
            Pair(7, Operation.Nop(-4)),
        )

        assertEquals(expected, result)

    }

    @Test
    fun `should run part 2`() {
        val lines =  getResourceAsText("/simple.txt")
        val result = runPart2(lines)

        assertEquals(8, result)
    }
}