package test

import createProgram
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import parseLine
import runPart1

internal class Part1KtTest {

    @Test
    fun `should parse line`() {
        assertEquals(Operation.Nop(0), parseLine("nop +0"))
        assertEquals(Operation.Acc(1), parseLine("acc +1"))
        assertEquals(Operation.Jump(4), parseLine("jmp +4"))
        assertEquals(Operation.Acc(3), parseLine("acc +3"))
        assertEquals(Operation.Jump(-3), parseLine("jmp -3"))
        assertEquals(Operation.Acc(-99), parseLine("acc -99"))
        assertEquals(Operation.Acc(1), parseLine("acc +1"))
        assertEquals(Operation.Jump(-4), parseLine("jmp -4"))
        assertEquals(Operation.Acc(6), parseLine("acc +6"))
    }

    @Test
    fun `should create program`() {
        val lines =  listOf(
            "nop +0",
            "acc +1",
            "jmp +4",
            "acc +3",
            "jmp -3",
            "acc -99",
            "acc +1",
            "jmp -4",
            "acc +6",
        )
        val expected = listOf(
            Operation.Nop(0),
            Operation.Acc(1),
            Operation.Jump(4),
            Operation.Acc(3),
            Operation.Jump(-3),
            Operation.Acc(-99),
            Operation.Acc(1),
            Operation.Jump(-4),
            Operation.Acc(6)
        )
        assertEquals(expected, createProgram(lines))
    }

    @Test
    fun `should run part 1`() {
        val lines =  listOf(
            "nop +0",
            "acc +1",
            "jmp +4",
            "acc +3",
            "jmp -3",
            "acc -99",
            "acc +1",
            "jmp -4",
            "acc +6",
        )
        assertEquals(5, runPart1(lines))
    }
}