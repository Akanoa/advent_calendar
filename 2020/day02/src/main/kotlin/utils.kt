data class LineData (
    val first: Int,
    val second: Int,
    val char : Char,
    val data: String
) {
    companion object {
        fun from(list: List<String>) = object {
            val min = list[0].toInt()
            val max = list[1].toInt()
            val char = list[2].single()

            val data = LineData(min, max, char, list[3])
        }.data
    }
}

fun parseLine(line: String) : LineData {

    val regex = Regex("^(\\d+)-(\\d+)\\s+([a-z]):\\s+([a-z]+)")
    return regex.findAll(line)
        .map { match -> match.groupValues.subList(1, 5) }
        .map{ x -> LineData.from(x) }
        .first()
}
