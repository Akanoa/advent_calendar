fun checkLine(line: LineData) : Boolean {

    return line.data
        .filter { it == line.char }
        .count() in line.first..line.second
}

fun runPart1(input: List<String>) : Int {

    return input
        .map { line -> parseLine(line)  }
        .filter { lineData -> checkLine(lineData) }
        .count()
}

fun part1() {

    val lines = readInputFile("day02/src/main/resources/input.txt")

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")

}