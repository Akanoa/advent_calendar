fun checkLine2(line: LineData) : Boolean {

    return (line.data[line.first - 1] == line.char).xor(line.data[line.second - 1] == line.char)
}

fun runPart2(input: List<String>) : Int {

    return input
        .map { line -> parseLine(line)  }
        .filter { lineData -> checkLine2(lineData) }
        .count()
}

fun part2() {

    val lines = readInputFile("day02/src/main/resources/input.txt")

    val result = runPart2(lines)
    println("Le résultat part 2 est $result")

}