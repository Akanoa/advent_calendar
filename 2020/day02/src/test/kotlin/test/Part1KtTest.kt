package test

import LineData
import checkLine
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import parseLine
import runPart1

internal class Part1KtTest {

    @Test
    fun shouldParseLine() {
        val line = "1-3 a: abcde"
        val expected = LineData(1, 3, 'a', "abcde")
        val result = parseLine(line)
        assertEquals(result, expected)

        val line2 = "1-3 b: cdefg"
        val expected2 = LineData(1, 3, 'b', "cdefg")
        val result2 = parseLine(line2)
        assertEquals(result2, expected2)

        val line3 = "2-9 c: ccccccccc"
        val expected3 = LineData(2, 9, 'c', "ccccccccc")
        val result3 = parseLine(line3)
        assertEquals(result3, expected3)

        val line4 = "13-19 z: zzzzzzzzzzzzzzzzzzzz"
        val expected4 = LineData(13, 19, 'z', "zzzzzzzzzzzzzzzzzzzz")
        val result4 = parseLine(line4)
        assertEquals(result4, expected4)
    }

    @Test
    fun `should run part 1`() {
        val line = "1-3 a: abcde"
        val line2 = "1-3 b: cdefg"
        val line3 = "2-9 c: ccccccccc"
        val lines =  listOf(line, line2,  line3)
        val expected = 2
        val result =  runPart1(lines)

        assertEquals(result, expected)
    }

    @Test
    fun shouldCheckLine() {
        val line = LineData(1, 3, 'a', "abcde")
        val expected = true
        val result = checkLine(line)
        assertEquals(result, expected)

        val line2 = LineData(1, 3, 'b', "cdefg")
        val expected2 = false
        val result2 = checkLine(line2)
        assertEquals(result2, expected2)

        val line3 = LineData(2, 9, 'c', "ccccccccc")
        val expected3 = true
        val result3 = checkLine(line3)
        assertEquals(result3, expected3)
    }
}