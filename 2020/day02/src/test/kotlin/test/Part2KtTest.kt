package test

import LineData
import checkLine2
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class Part2KtTest {

    @Test
    fun `should check line part 2`() {
        val line = LineData(1, 3, 'a', "abcde")
        val expected = true
        val result = checkLine2(line)
        assertEquals(result, expected)

        val line2 = LineData(1, 3, 'b', "cdefg")
        val expected2 = false
        val result2 = checkLine2(line2)
        assertEquals(result2, expected2)

        val line3 = LineData(2, 9, 'c', "ccccccccc")
        val expected3 = false
        val result3 = checkLine2(line3)
        assertEquals(result3, expected3)
    }
}