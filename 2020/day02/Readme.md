# [Jour 2](https://adventofcode.com/2020/day/2)

## Partie 1

### Problème
On dispose dispose d'un mot de passe et on doit déterminer si celui-ci est juste ou non.

Les règle de validation sont définies comme suit:
`1-3 a: abcde`

`1-3` : définit l'intervalle d'occurence d'un certain caractère, la borne minimale à gauche du `-` et la borne maximale à sa droite

Le carctère vérifié est séparé par un espace, ici `a`

Puis vient un `: `

Et finalement le mot de passe en question: `abcde`

On peut reformuler cette ligne en `{min}-{max} {char}: {password}`

Selon ces règles de validation:

- `1-3 a: abcde` est valide
- `1-3 b: cdefg` est invalide, car `b` n'est pas présent
- `2-9 c: ccccccccc` est invalide, car `c` est présent en 10 occurences qui est au delà de la borne max

### Résolution

Ce problème est résolvable via une [regex:](https://regexr.com/) `^(\\d+)-(\\d+)\\s+([a-z]):\\s+([a-z]+)`.

Pour ce qui ne connaisse pas le principe des regex il s'agit d'un formalisme qui permet de traiter des chaines de caractères et de vérifier
leur validité mais aussi de capturer des morceaux de cette chaîne de caractères.

Dans le formalisme regex, une partie entre parenthèses est appelé un groupe et tout ce qui est dans ce groupe peut-être traité.

Une fois les groupes capturés, on peut traiter les bornes {min} et {max} pour les transformer en entiers, la regex nous assure que ces deux groupes capturé
sont bel et bien des nombres et donc que l'on peut les parser en entiers.

Ensuite il faut compter les occurences du `char` dans `password`.

Si le nombre d'occurence est compris dans l'intervalle `[min, max]` le mot de passe est correcte sinon non.

Ensuite on compte les mots de passes valides et on renvoit le résultat.

### Qu'est ce que j'ai appris

A créer une regex et a utiliser les groupes capturants en Kotlin.

## Partie 2

## Problème

On possède le même genre de chaîne de caractères de format `{min}-{max} {char}: {password}`.

Mais ici `min` prend le sens d'index de la première occurence de `char` vérifiée et `max` de la deuxième occurence de `char`.

On reformule donc la chaîne en `{first}-{second} {char}: {password}`.

La règle de validation ne porte plus ici sur le nombre d'occurence de `char` mais sur son unicité entre les caractères `first` et `second`.

- `1-3 a: abcde`, est valide car `a` est présent caractère en position `1` mais pas en position `3` qui est un `c`
- `1-3 b: cdefg`, est invalide car `b` n'est ni présent en position `1`, ni en position `3`
- `2-9 c: ccccccccc`, est invalide car `c` est présent à la fois en position `2` et `9`

### Résolution

On utilise la même regex pour récupérer les composantes `first` et `second` et on les transforme de la même manière en entiers.

Ensuite on récupère les caractères en position `first` et `second`.

Il faut qu'au moins l'un des deux caractères soit `char`, mais pas les deux.

Ce qui est exactement le principe de l'opérateur logique [xor](https://www.positron-libre.com/cours/electronique/logique-combinatoire/fonctions-logiques/fonction-ou-exclusif-xor.php)

Dont voici la table de vérité.

| first | second | bon mot de passe |
|:---:|:---:|:---:|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 0 |

On applique donc l'opérateur `xor` sur l'expression `validFirst =  first == char` et `validSecond = second == char`.

Pour que le mot de passe soit valide `validFirst xor validSecond` doit être vrai.

On compte le nombre de mots de passes valides et retourne ce nombre.

### Qu'est ce que j'ai appris

L'utilisation des opérateurs logiques en Kotlin.

## Conclusion

Un puzzle sympa pour la manipulation des expressions régulières.

## Puzzle mark

 ⭐⭐☆☆☆
