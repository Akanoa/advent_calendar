import java.io.File

fun readInputFile(pathname: String) : List<String> {
    return File(pathname)
        .readLines()
}

fun getResourceAsText(path: String): List<String> {
    return object {}.javaClass.getResource(path)
        .readText()
        .split("\n")
}