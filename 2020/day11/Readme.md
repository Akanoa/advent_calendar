# [Jour 11](https://adventofcode.com/2020/day/11)

## Partie 1

### Problème

Il s'agit d'une sorte de jeu de la vie de Conway.

Le monde est découpé en cellules. Ces cellules on trop 3 états:

- Libre (L)
- Occupée (#) 
- Inaccessible (.)

Les règles qui régissent l'évolution sont:

- Si la cellule est libre et qu'aucune cellules adjacentes est occupée, alors
la cellule devient occupée.
- Si la cellule est occupée et que 4 ou plus cases adjacentes sont occupées, la
cellule devient libre.
- Si la cellule est inaccessible alors elle le demeure.  
- Sinon, aucun changement


Les cellules sont indépendantes. Chaque itération ne prend en compte que la cellule
actuelle et les cellules adjacentes de la précédente itération.

Le comportement est volontairement chaotique mais il existe un point de stabilisation
où les changements cessent.

Le but du puzzle est de déterminer le numéro de l'itération de se point de bascule.

### Résolution

La première chose à faire est de déterminer les cases adjacentes à une case en particulier.

Pour cela il existe un algorithme plutôt élégant :

Soit map le tableau 2D du monde de dimension w x h

Soit (x, y) les coordonnées de la cellules courantes

```
ajdacentents = []
Pour  i  de 0 à 9
    if i != 4
        adj = map[x + i//3 - 1][y + i%3 - 1]
        Si adj existe 
            adjacentents.add(adj)     
```

### Qu'est ce que j'ai appris

## Partie 2

## Problème

### Résolution

### Qu'est ce que j'ai appris

## Conclusion

## Puzzle mark

⭐⭐⭐☆☆