fun runPart2(lines: List<String>) : Int {

    return 0
}

fun part2() {
    val lines = readInputFile("day08/src/main/resources/input.txt")

    val result = runPart2(lines)
    println("Le résultat part 1 est $result")
}