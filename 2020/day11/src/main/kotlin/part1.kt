typealias Coord = Pair<Int, Int>
typealias MapDimension = Pair<Int, Int>

enum class State {
    Empty,
    Occupied,
    Void
}

fun parseState(char: Char) : State {
    return when(char) {
        '.' -> State.Void
        '#' -> State.Occupied
        'L' -> State.Empty
        else -> throw Error("Unexpected character")
    }
}

fun getAdjacentMap(map: MapDimension) : HashMap<Coord, List<Coord>> {

    val hashMap = hashMapOf<Coord, List<Coord>>()

    for (y in 0 until map.second) {
        for (x in 0 until map.first) {
            val coord = Coord(x, y)
            hashMap[coord] = getAdjacent(map, coord)
        }
    }

    return hashMap
}

fun getMapDimensionFromLines(lines: List<String>) : MapDimension {
    return MapDimension(lines[0].chars().count().toInt(), lines.size)
}

fun getStateMap(lines: List<String>) : Array<Array<State>> {

    val mapDimension = getMapDimensionFromLines(lines)

    val map = Array(mapDimension.second) {Array(mapDimension.first) {State.Empty} }

    for ((y, line) in lines.withIndex()) {

        for ((x, char) in line.toCharArray().withIndex()) {
            val state = parseState(char)
            map[y][x] = state
        }
    }

    return map
}

fun getAdjacent(map: MapDimension, coord: Coord) : List<Coord> {
    val adjacents = mutableListOf<Coord>()
    for(i in 0..9) {
        if (i != 4) {
            val x = coord.first + i / 3 - 1
            val y = coord.second + i % 3 - 1
            if(x in 0 until map.first && y in 0 until map.second) {
                adjacents.add(Coord(x, y))
            }
        }
    }

    return adjacents.toList()
}


fun runPart1(lines: List<String>) : Int {

    val mapDimension = getMapDimensionFromLines(lines)
    val adjacents = getAdjacentMap(mapDimension)

    var map = getStateMap(lines)

    val mapHash = map.hashCode()

    while (true) {

        for(line in map) {
            for (cell in line) {

            }
        }

        if (mapHash == map.hashCode()) {
            break
        }
    }

    return 0
}

fun part1() {
    val lines = readInputFile("day11/src/main/resources/input.txt")

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")
}