package test

import Coord
import MapDimension
import getAdjacent
import getAdjacentMap
import getStateMap
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Part1KtTest {

    @Test
    fun `should get adjacent simple`() {
        val mapDimension = Coord(3, 3)
        val cur = Coord(1, 1)

        val expected = listOf(
            Coord(0, 0),
            Coord(1, 0),
            Coord(2, 0),
            Coord(0, 1),
            Coord(2, 1),
            Coord(0, 2),
            Coord(1, 2),
            Coord(2, 2),
        )

        assertEquals(expected.toHashSet(), getAdjacent(mapDimension, cur).toHashSet())

    }

    @Test
    fun `should get adjacent outer border (2, 2)`() {
        val mapDimension = Coord(3, 3)
        val cur = Coord(2, 2)

        val expected = listOf(
            Coord(1, 1),
            Coord(1, 2),
            Coord(2, 1),
        )

        assertEquals(expected.toHashSet(), getAdjacent(mapDimension, cur).toHashSet())

    }

    @Test
    fun `should get adjacent outer border (0, 0)`() {
        val mapDimension = Coord(3, 3)
        val cur = Coord(0, 0)

        val expected = listOf(
            Coord(1, 1),
            Coord(0, 1),
            Coord(1, 0),
        )

        assertEquals(expected.toHashSet(), getAdjacent(mapDimension, cur).toHashSet())

    }

    @Test
    fun `should get adjacent outer border (1, 2)`() {
        val mapDimension = Coord(3, 3)
        val cur = Coord(1, 2)

        val expected = listOf(
            Coord(0, 1),
            Coord(1, 1),
            Coord(2, 1),
            Coord(0, 2),
            Coord(2, 2),
        )

        assertEquals(expected.toHashSet(), getAdjacent(mapDimension, cur).toHashSet())

    }

    @Test
    fun `should get map of state`() {
        val lines = listOf(
            "#.L.",
            "#.#L",
        )
        val expected = arrayOf(
            arrayOf(State.Occupied, State.Void, State.Empty, State.Void),
            arrayOf(State.Occupied, State.Void, State.Occupied, State.Empty),
        )
        assertArrayEquals(expected, getStateMap(lines))
    }

    @Test
    fun `should get adjacence map`() {
        val mapDimension = MapDimension(2, 2)
        val expected = hashMapOf(
            Pair(Coord(0, 0), listOf(Coord(0, 1), Coord(1, 0), Coord(1, 1))),
            Pair(Coord(1, 0), listOf(Coord(0, 0), Coord(0, 1), Coord(1, 1))),
            Pair(Coord(0, 1), listOf(Coord(0, 0), Coord(1, 0), Coord(1, 1))),
            Pair(Coord(1, 1), listOf(Coord(0, 0), Coord(0, 1), Coord(1, 0))),
        )
        assertEquals(expected, getAdjacentMap(mapDimension))
    }

    @Test
    fun `should run part 1`() {

    }
}