fun getQuestionNumberAnsweredAtLeastOnce(line: String) : Long {

    return line
        .split("\n")
        .joinToString("")
        .chars()
        .distinct()
        .count()
}

fun runPart1(lines: List<String>) : Long {
    return lines
        .joinToString("") { line -> line + "\n" }
        .split("\n\n")
        .map { line -> getQuestionNumberAnsweredAtLeastOnce(line) }
        .sum()
}

fun part1() {
    val lines = readInputFile("day06/src/main/resources/input.txt")

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")
}