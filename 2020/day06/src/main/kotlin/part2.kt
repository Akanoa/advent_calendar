fun getQuestionNumberAnsweredByEveryone(group: String) : Int {


    val lines =  group.split("\n").filter { x -> x != "" }

    var final = lines.first().chars().distinct().toArray().asList().toIntArray()

    for(line in lines) {

        val chars = line.chars().distinct().toArray().asList().toIntArray()
        final = final.intersect(chars.asIterable()).toIntArray()
    }

    return final.size
}

fun runPart2(lines: List<String>) : Int {
    return lines
        .joinToString("") { line -> line + "\n" }
        .split("\n\n")
        .map { line -> getQuestionNumberAnsweredByEveryone(line) }
        .sum()
}

fun part2() {
    val lines = readInputFile("day06/src/main/resources/input.txt")

    val result = runPart2(lines)
    println("Le résultat part 2 est $result")
}