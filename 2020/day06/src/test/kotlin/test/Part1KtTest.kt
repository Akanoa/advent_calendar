package test

import getQuestionNumberAnsweredAtLeastOnce
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart1

internal class Part1KtTest {

    @Test
    fun `should get question amount answered by at least one`() {
        assertEquals(getQuestionNumberAnsweredAtLeastOnce("abcx"), 4)
        assertEquals(getQuestionNumberAnsweredAtLeastOnce("abcy"), 4)
        assertEquals(getQuestionNumberAnsweredAtLeastOnce("abcz"), 4)
        assertEquals(getQuestionNumberAnsweredAtLeastOnce("abcx\nabcy\nabcz"), 6)
    }

    @Test
    fun `should run part 1`() {
        val lines = listOf(
            "abc",
            "",
            "a",
            "b",
            "c",
            "",
            "ab",
            "ac",
            "",
            "a",
            "a",
            "a",
            "a",
            "",
            "b"
        )
        assertEquals(runPart1(lines), 11)
    }
}