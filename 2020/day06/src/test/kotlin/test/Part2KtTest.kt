package test

import getQuestionNumberAnsweredByEveryone
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart2

internal class Part2KtTest {
    @Test
    fun `should get amount of question answered by everybody`() {
        assertEquals(3, getQuestionNumberAnsweredByEveryone("abc"))
        assertEquals(0, getQuestionNumberAnsweredByEveryone("a\nb\nc"))
        assertEquals(1, getQuestionNumberAnsweredByEveryone("ab\nac"))
        assertEquals(1, getQuestionNumberAnsweredByEveryone("a\na\na\na"))
        assertEquals(1, getQuestionNumberAnsweredByEveryone("b\n"))
    }

    @Test
    fun `should run part 2`() {
        val lines = listOf(
            "abc",
            "",
            "a",
            "b",
            "c",
            "",
            "ab",
            "ac",
            "",
            "a",
            "a",
            "a",
            "a",
            "",
            "b"
        )
        assertEquals(6, runPart2(lines))
    }
}