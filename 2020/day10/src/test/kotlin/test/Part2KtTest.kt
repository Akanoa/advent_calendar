package test

import getResourceAsText
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart2

internal class Part2KtTest {

    @Test
    fun `should run part 2`() {

        val lines = getResourceAsText("/simple.txt").map { it.toLong() }
        assertEquals(8, runPart2(lines))

        val lines2 = getResourceAsText("/larger.txt").map { it.toLong() }
        assertEquals(19208, runPart2(lines2))
    }
}