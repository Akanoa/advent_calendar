package test

import generateLowerRange
import generateMap
import getResourceAsText
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart1

internal class Part1KtTest {

    @Test
    fun `should generate lower range`() {
        assertEquals(hashSetOf(0), generateLowerRange(1, 3, 19))
        assertEquals(hashSetOf(0, 1), generateLowerRange(2, 3, 19))
        assertEquals(hashSetOf(3, 2, 1), generateLowerRange(4, 3, 19))
        assertEquals(hashSetOf(11, 10, 9), generateLowerRange(12, 3, 19))
        assertEquals(hashSetOf(18, 17, 16), generateLowerRange(19, 3, 19))
        assertEquals(hashSetOf(20, 21, 22), generateLowerRange(20, 3, 19))
    }

    @Test
    fun `should generate map`() {
        val list = listOf(
            16,
            10,
            15,
            5,
            1,
            11,
            7,
            19,
            6,
            12,
            4
        )
        val expected = hashMapOf(
            Pair(0, mutableListOf(1)),
            Pair(1, mutableListOf(4)),
            Pair(4, mutableListOf(5, 6, 7)),
            Pair(5, mutableListOf(6, 7)),
            Pair(6, mutableListOf(7)),
            Pair(7, mutableListOf(10)),
            Pair(10, mutableListOf(11, 12)),
            Pair(11, mutableListOf(12)),
            Pair(12, mutableListOf(15)),
            Pair(15, mutableListOf(16)),
            Pair(16, mutableListOf(19)),
            Pair(19, mutableListOf(22)),
        )

        assertEquals(expected, generateMap(list, 3))
    }

    @Test
    fun `should run part 1`() {
        val lines = getResourceAsText("/simple.txt").map { line -> line.toInt() }
        val result = runPart1(lines)
        assertEquals(35, result)

        val lines2 = getResourceAsText("/larger.txt").map { line -> line.toInt() }
        val result2 = runPart1(lines2)
        assertEquals(220, result2)
    }
}