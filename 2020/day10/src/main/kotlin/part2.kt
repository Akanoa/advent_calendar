fun runPart2(adaptersWithoutBuildIn: List<Long>) : Long {

    val arrangements = hashMapOf<Long, Long>()
    arrangements[0] = 1

    val maxJolts = adaptersWithoutBuildIn.maxOrNull() ?: throw Error("No max found")

    val adapters = adaptersWithoutBuildIn.toHashSet()
    adapters.add(0)

    for (adapter in adapters.sorted()) {
        for (joltDifference in 1..3) {
            val next = adapter + joltDifference
            if (adapters.contains(next)) {
                if (!arrangements.containsKey(next)) {
                    arrangements[next] = 0
                }
                val previousArrangementsValue = arrangements[next]
                if (previousArrangementsValue != null) {
                    arrangements[next] = previousArrangementsValue + arrangements[adapter]!!
                }
            }

        }
    }

    return arrangements[maxJolts] ?: throw Error("Missing arrangements key")
}

fun part2() {
    val lines = readInputFile("day10/src/main/resources/input.txt").map { it.toLong() }

    val result = runPart2(lines)
    println("Le résultat part 2 est $result")
}