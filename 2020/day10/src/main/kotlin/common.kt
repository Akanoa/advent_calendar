fun generateLowerRange(number: Int, rangeSize: Int, max: Int) : HashSet<Int> {

    if (number > max ) {
        return (max + 1  until max + 4 ).toHashSet()
    }

    val lower = if (number < rangeSize) 0 else number - rangeSize

    return (number - 1 downTo lower).toHashSet()
}

fun generateMap(list: List<Int>, rangeSize: Int) : HashMap<Int, MutableList<Int>> {

    val max = list.maxOrNull() ?: throw Error("Unable to find max adapter")

    val workList = list.toHashSet()
    workList.add(0)

    val hasMap =  workList
        .sorted()
        .fold(hashMapOf<Int, MutableList<Int>>()) { acc, adapter -> acc.also {
            generateLowerRange(adapter, rangeSize, max).forEach {value ->
                run {

                    if(workList.contains(value) || value >= max) {

                        if (!acc.containsKey(value)) {
                            acc[value] = mutableListOf()
                        }
                        acc[value]?.add(adapter)
                    }

                }
            }

        }  }

    hasMap[max] = mutableListOf(max + rangeSize)

    return hasMap

}