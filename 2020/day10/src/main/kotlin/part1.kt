fun runPart1(lines: List<Int>) : Int {

    var counter1 = 0
    var counter3 = 0

    val hashMap = generateMap(lines, 3)
    hashMap.entries.forEach { entry -> run {
        when(entry.value.minOrNull()?.minus(entry.key)) {
            1 -> counter1++
            3 -> counter3++
        }
    } }

    return counter3*counter1
}

fun part1() {
    val lines = readInputFile("day10/src/main/resources/input.txt")
        .map { line -> line.toInt() }

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")
}