# [Jour 10](https://adventofcode.com/2020/day/10)

## Partie 1

### Problème

On possède une liste de nombre entiers.

On a un intervalle de liberté d’au plus -3 par rapport à chacune des valeurs

Donc `6` par exemple peut aussi être considéré comme un `5`, `4` ou un `3`

Le nombre le plus élevé de la liste peut-être considérée comme ayant une valeur de +3

```
1 -> 0
4 -> 3, 2, 1
5 -> 4, 3, 2
6 -> 5, 4, 3
7 -> 6, 5, 4
10 -> 9, 8, 7
11 -> 10, 9, 8
12 -> 11, 10, 9
15 -> 14, 13, 12
16 -> 15, 14, 13
19 -> 18, 17, 16 => 19 + 3 = 22
```
On inverse la map

```
0 -> 1          => 1
1 -> 4          => 3
4 -> 5, 6, 7    => 1
5 -> 6, 7       => 1 
6 -> 7          => 1
7 -> 10         => 3
10 -> 11, 12    => 1
11 -> 12        => 1
12 -> 15        => 3
15 -> 16        => 1
16 -> 19        => 3
19 -> 22        => 3
```

Pour chaque valeur de 0 à 19 (valeur max) on cherche à trouver la clef de valeur
minimale qui possède dans son intervalle la valeur d'itération.

On doit donner le produit du nombre d'écarts de 3 multiplié par le nombre d'écarts de 1.

Ici 7 `1` et 5 `3`, la réponse est `35`

### Résolution

### Qu'est ce que j'ai appris

## Partie 2

## Problème

### Résolution

### Qu'est ce que j'ai appris

## Conclusion

## Puzzle mark

⭐⭐⭐☆☆









































A = List<Int>
B = List<Var>

A x C = List<Pair<Int, Var>>

A = [1, 2]
B = [a, b]
C = ["t", "c"]

A x B x C = (A x B) x C

A x B = [ (1, a), (1, b), (2, a), (2, b) ]

(A x B) x C = [ (1, a), (1, b), (2, a), (2, b) ] x [ "t", "c" ]
            = [
                [ (1, a), "t" ],
                [ (1, a), "c" ],
                [ (1, b), "t" ],
                [ (1, b), "c" ],
                [ (2, a), "t" ],
                [ (2, a), "c" ],
                [ (2, b), "t" ],
                [ (2, b), "c" ],

            ]           

            = [
                [ 1, a, "t" ],
                [ 1, a, "c" ],
                [ 1, b, "t" ],
                [ 1, b, "c" ],
                [ 2, a, "t" ],
                [ 2, a, "c" ],
                [ 2, b, "t" ],
                [ 2, b, "c" ],

            ]     






























