# Advent of Code en Kotlin 

En cette belle année de pandémie, j'ai choisi de réaliser le challenge en Kotlin.

Je suis actuellement en apprentissage du Java, et donc en parrallèle il m'a semblé intéressant de 
me former sur le [Kotlin](https://kotlinlang.org/)

Je suis un débutant total dans le langage et donc je vais surement commettre des erreurs grossières.

## Contributions

Sentez vous libre de proposer des améliorations, les merges requets sont là pour ça 😀

## Lab

Cette année et en exclusivité je me suis lancé dans la construction d'un dépôt [lab](https://gitlab.com/akanoa-test/avent_of_code_rd) où j'expérimenterai
divers techniques, concepts, outils ou whatever. 
