# [Jour 1](https://adventofcode.com/2020/day/1)

## Partie 1

### Problème

Vous disposez d'une liste finie d'entiers uniques et vous souhaitez trouver le duo d'élement qui lorsqu'on les somme font 2020.

Exemple:

pour la liste : `1721;979;366;299;675;1456`

Le duo est `1721` et `299`

Une fois le duo trouvé il faut renvoyer sont produit.

### Résolution

Une simpler double boucle pour réaliser le produit cartésien.

Complexité O(n^2)

### Qu'est ce que j'ai appris

A faire un boucle et un condition de sortie de boucle.

A mettre en place un projet Kotlin merci au viewers d'ailleurs🙏, sans eux je serai resté à l'étape de setup ^^

J'ai écrit un petit exemple pour setup un projet multi module.

## Partie 2

La même chose que la partie 1 mais avec un triplet de nombre.

### Résolution

Trible boucle imbriquée.

Complexité : O(n^3)

### Bonus
J'ai travaillé à une optimisation mais cette fois ci en [Rust](https://gitlab.com/akanoa-test/avent_of_code_rd/-/tree/master/2020/day01). Elle permet de diminuer la
complexité en O(n^2).

### Qu'est ce que j'ai appris

Pas grand chose de plus

## Conclusion

Intéressant pour setup le dépôts Kotlin en modules.

## Puzzle mark

 ⭐☆☆☆☆
