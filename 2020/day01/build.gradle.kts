plugins {
    java
    kotlin("jvm") version "1.4.10"
}

group = "me.noa"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(project(":common"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.0")
    testImplementation ("org.junit.jupiter:junit-jupiter-engine:5.7.0")
    testImplementation ("org.junit.jupiter:junit-jupiter-params:5.7.0")
}

tasks.withType<Test> {
    useJUnitPlatform()
}