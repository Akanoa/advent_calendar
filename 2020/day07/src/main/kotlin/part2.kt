fun walk(node: Node, graph: Graph, numberOfBags: Long = 0) : Long {
    var acc = numberOfBags
    for (child in node.children) {
        val childNode = graph[child.first] ?: throw Error("Not found")
        acc += child.second + child.second * walk(childNode, graph)
    }

    return acc
}

fun runPart2(lines: List<String>) : Long {

    val graph = createGraph(lines)
    val shinyGoldNode = graph["shiny gold"] ?: throw Error("Shiny Gold node not found")

    return walk(shinyGoldNode, graph)

}

fun part2() {
    val lines = readInputFile("day07/src/main/resources/input.txt")

    val result = runPart2(lines)
    println("Le résultat part 2 est $result")
}