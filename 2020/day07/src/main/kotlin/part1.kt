fun walk(node: Node, graph: Graph, hashSet: HashSet<String>) : HashSet<String> {

    for(parent in node.parents) {
        hashSet.add(parent)
        val parentNode = graph[parent] ?: throw Error("Not found")
        walk(parentNode, graph, hashSet)
    }

    return hashSet
}

fun runPart1(lines: List<String>) : Int {
    val graph = createGraph(lines)
    val shinyGoldNode = graph["shiny gold"] ?: throw Error("Shiny Gold node not found")

    return walk(shinyGoldNode, graph, hashSetOf()).size
}

fun part1() {
    val lines = readInputFile("day07/src/main/resources/input.txt")

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")
}