import java.lang.Error

data class Node(
    val children: HashSet<Relation> = hashSetOf(),
    val parents: HashSet<String> = hashSetOf()
)

typealias Relation = Pair<String, Int>
typealias Graph = HashMap<String, Node>


fun parseLine(line: String) : Pair<String, Node> {

    val regexLine = Regex("^(\\w+\\s\\w+)\\sbags\\scontain\\s(.*)\\.")
    val regexChild = Regex("(\\d+)\\s(\\w+\\s\\w+).*")
    val match = regexLine.matchEntire(line)?.groupValues?.drop(1) ?: throw Error("Malformed line")

    val children = match[1]
        .split(",")
        .asSequence()
        .map { component -> component.trim() }
        .filter { component -> component != "no other bags" }
        .map { component -> regexChild.matchEntire(component)?.groupValues?.drop(1) }
        .mapNotNull { component -> component?.let { Pair(it[1], it[0].toInt()) } }
        .fold(hashSetOf<Relation>()) { acc, pair ->  acc.also { acc.add(pair) }}


    return Pair(match[0], Node(children))
}

fun createGraph(lines: List<String>) : HashMap<String, Node>  {

    val graph: Graph = hashMapOf()

    for (line in lines) {
        val nodeData = parseLine(line)
        if (!graph.containsKey(nodeData.first)) {
            graph[nodeData.first] = Node()
        }

        for (relation in nodeData.second.children) {
            if (!graph.containsKey(relation.first)) {
                graph[relation.first] = Node(hashSetOf(), hashSetOf(nodeData.first))
            }

            val relationData = graph[relation.first]
            relationData?.parents?.add(nodeData.first)
            graph[nodeData.first]?.children?.add(relation)

        }
    }

    return graph

}
