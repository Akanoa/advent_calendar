package test

import getResourceAsText
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart2

internal class Part2KtTest {
    @Test
    fun `should run part 2`() {
        val lines = getResourceAsText("/simple.txt")
        val result = runPart2(lines)
        assertEquals(32, result)

        val lines2 = getResourceAsText("/part2.txt")
        val result2 = runPart2(lines2)
        assertEquals(126, result2)
    }

}