package test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import Node
import Relation
import createGraph
import getResourceAsText
import parseLine
import runPart1

internal class Part1KtTest {

    @Test
    fun `should parse a line`() {

        assertEquals(Pair("light red", Node(hashSetOf(
            Pair("bright white", 1),
            Pair("muted yellow", 2),
        ))
        ), parseLine("light red bags contain 1 bright white bag, 2 muted yellow bags."))

        assertEquals(Pair("dark orange", Node(hashSetOf(
            Pair("bright white", 3),
            Pair("muted yellow", 4),
        )
        )), parseLine("dark orange bags contain 3 bright white bags, 4 muted yellow bags."))

        assertEquals(Pair("bright white", Node(hashSetOf(
            Pair("shiny gold", 1),
        ))
        ), parseLine("bright white bags contain 1 shiny gold bag."))

        assertEquals(Pair("muted yellow", Node(hashSetOf(
            Pair("shiny gold", 2),
            Pair("faded blue", 9),
        ))
        ), parseLine("muted yellow bags contain 2 shiny gold bags, 9 faded blue bags."))

        assertEquals(Pair("shiny gold", Node(hashSetOf(
            Pair("dark olive", 1),
            Pair("vibrant plum", 2),
        ))
        ), parseLine("shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags."))

        assertEquals(Pair("dark olive", Node(hashSetOf(
            Pair("faded blue", 3),
            Pair("dotted black", 4),
        ))
        ), parseLine("dark olive bags contain 3 faded blue bags, 4 dotted black bags."))

        assertEquals(Pair("vibrant plum", Node(hashSetOf(
            Pair("faded blue", 5),
            Pair("dotted black", 6),
        ))
        ), parseLine("vibrant plum bags contain 5 faded blue bags, 6 dotted black bags."))

        assertEquals(Pair("faded blue", Node(hashSetOf())),
            parseLine("faded blue bags contain no other bags."))

        assertEquals(Pair("dotted black", Node(hashSetOf())),
            parseLine("dotted black bags contain no other bags."))
    }

    @Test
    fun `should create graph`() {
        val lines = getResourceAsText("/simple.txt")


        val expected = hashMapOf<String, Node>(
            Pair("light red", Node(children = hashSetOf(
                Relation("bright white", 1),
                Relation("muted yellow", 2)
            ))),
            Pair("muted yellow", Node(children = hashSetOf(
                Relation("shiny gold", 2),
                Relation("faded blue", 9)
            ), parents = hashSetOf("dark orange", "light red"))),
            Pair("bright white", Node(children = hashSetOf(
                Relation("shiny gold", 1),
            ), parents = hashSetOf("dark orange", "light red"))),
            Pair("dark orange", Node(children = hashSetOf(
                Relation("bright white",3),
                Relation("muted yellow", 4)
            ))),
            Pair("shiny gold", Node(children = hashSetOf(
                Relation("dark olive", 1),
                Relation("vibrant plum", 2)
            ), parents = hashSetOf("bright white", "muted yellow"))),
            Pair("dark olive", Node(children = hashSetOf(
                Relation("dotted black", 4),
                Relation("faded blue", 3)
            ), parents = hashSetOf("shiny gold"))),
            Pair("dotted black", Node(children = hashSetOf(),
                parents = hashSetOf("dark olive", "vibrant plum"))),
            Pair("vibrant plum", Node(children = hashSetOf(
                Relation("dotted black", 6),
                Relation("faded blue", 5)
            ), parents = hashSetOf("shiny gold"))),
            Pair("faded blue", Node(children = hashSetOf(),
                parents = hashSetOf("muted yellow", "vibrant plum", "dark olive")))

        )

        val result = createGraph(lines)
        assertEquals(expected, result)
    }

    @Test
    fun `should run part 1`() {
        val lines = getResourceAsText("/simple.txt")
        val result = runPart1(lines)
        assertEquals(4, result)
    }

}