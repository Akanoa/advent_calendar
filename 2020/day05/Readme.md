# [Jour 5](https://adventofcode.com/2020/day/5)

## Partie 1

### Problème

Dans ce problème on dispose d'une chaîne de caractère du type `FBFBBFFRLR`.
On peut décomposer en deux morceaux `FBFBBFF` et `RLR`.

On va d'abord détailler le comportement de la première partie.

On part d'un intervalle de [0-127].

La chaîne `FBFBBFF` elle-même est composée de `F` et de `R`.

En fonction du caractère `F` ou `R` on subdivise l'intervalle comme suit:

![](./assets/images/day5-part1-graph.png)

En déroulant cet algorithme on arrive avec la chaîne `FBFBBFF` on arrive à `44`.

La première chaîne fait toujours 7 caractères et la deuxième toujours 3 caractères.

D'ailleurs la deuxième partie fonctionne ainsi :

- `F` devient `L`
- `B` devient `R`
- l'intervalle `[0-127]` devient `[0-7]`

![](./assets/images/day5-part1-graph-2.png)

On déroule le même algo avec la chaîne `RLR` on arrive à `5`.

Pour obtenir le résultat final on prend le premier résultat r1, le deuxième résultat r2

Et on effectue le calcul ainsi: `résultat = r1 * 8 + r2`

Le puzzle fourni une liste de chaîne de caractère.

Le but est de retrouver le résultat le plus haut.

### Résolution

La partie la plus complexe dans la résolution du problème est de déterminer les enfants du noeud courant.

![](./assets/images/day5-part1-graph-algo.png)

L'algo se termine lorsque soit `max - min = 0`.

On applique pour chaque chaîne du fichier.

On renvoie le max de la liste.

### Qu'est ce que j'ai appris

Comment utiliser des librairies externes via gradle. 

```kotlin
dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}
```

En particulier de l'assertion d'erreur déclenchée par une fonction.
```kotlin
assertFailsWith<Error> {
    willFail()
}
```

## Partie 2

## Problème

Le but est de trouver le nombre qui n'est pas présent dans la liste des chaînes une fois passer
par l'algorithme.

### Résolution

On reprend la liste des résultats et au lieu d'en rechercher le maximum.

On trie les résultats par ordre croissant.

Puis on recherche un élément du tableau tel que son successeur n'est pas égal à `élément + 1`

On renvoie `élément + 1`

## Conclusion

Problème intéressant que j'ai résolu de manière bien trop compliqué ^^

## Puzzle mark

⭐⭐⭐☆☆
