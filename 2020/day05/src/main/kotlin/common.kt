import java.lang.Error

data class Seat(val row: Int, val column: Int) {

    fun getSeatId() : Int {
        return this.row * 8 + column
    }
}

data class Bounds(val min1: Int, val max1: Int, val min2: Int, val max2: Int) {

    companion object {
        fun getNextBounds(min: Int, max: Int) : Bounds {
            val min2 = min + ( ( max - min ) + 1 ) / 2
            val max1 = min2 - 1
            return Bounds(min, max1, min2, max)
        }
    }
}

enum class Side {
    Left,
    Right
}

fun charToSide(char: Char) : Side {

    return when(char) {
        'F' -> Side.Left
        'B' -> Side.Right
        'L' -> Side.Left
        'R' -> Side.Right
        else -> throw Error("Unable to parse path component")
    }
}

fun binarySearch(min: Int, max: Int, path: String) : Int {

    var currentMin = min
    var currentMax = max

    for (char in path) {
        val bounds = Bounds.getNextBounds(currentMin, currentMax)
        when(charToSide(char)) {
            Side.Left -> {
                currentMin = bounds.min1
                currentMax = bounds.max1
            }
            Side.Right -> {
                currentMin = bounds.min2
                currentMax = bounds.max2
            }
        }
    }

    return currentMin
}

fun getSeat(path: String) : Seat {

    val row = binarySearch(0, 127, path.substring(0, 7))
    val column = binarySearch(0, 7, path.substring(7))
    return Seat(row, column)
}