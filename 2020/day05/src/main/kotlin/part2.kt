import java.lang.Error

fun foundElementWithoutSuccessor(tab: List<Int>) : Int? {

    val sortedTab = tab.sorted()

    for( (i,x) in sortedTab.withIndex()) {

        if(i == sortedTab.size) {
            break
        }

        if (x + 1 != sortedTab[i + 1]) {
            return x + 1
        }
    }

    return null
}


fun runPart2(lines: List<String>) : Int {

    val seats = lines
        .asSequence()
        .map { path -> getSeat(path) }
        .map { seat -> seat.getSeatId() }
        .toList()

    return foundElementWithoutSuccessor(seats) ?: throw Error("No boarding pass found sorry")
}

fun part2() {
    val lines = readInputFile("day05/src/main/resources/input.txt")

    val result = runPart2(lines)
    println("Le résultat part 2 est $result")
}