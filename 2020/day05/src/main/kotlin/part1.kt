fun runPart1(lines: List<String>) : Int {
    return lines
        .map { path -> getSeat(path).getSeatId() }
        .sorted()
        .reversed()
        .first()
}

fun part1() {
    val lines = readInputFile("day05/src/main/resources/input.txt")

    val result = runPart1(lines)
    println("Le résultat part 1 est $result")
}