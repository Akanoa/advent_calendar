package test

import foundElementWithoutSuccessor
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Part2KtTest {

    @Test
    fun `should return the number without successor`() {
        val tab = listOf(
            4,
            3,
            2,
            5,
            7
        )
        assertEquals(6, foundElementWithoutSuccessor(tab))
    }
}