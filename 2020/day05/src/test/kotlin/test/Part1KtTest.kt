package test

import Bounds
import binarySearch
import charToSide
import getSeat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import runPart1
import kotlin.test.assertFailsWith

internal class Part1KtTest {

    @Test
    fun `should get the next node bounds`() {
        assertEquals(Bounds.getNextBounds(0, 127), Bounds(0, 63, 64, 127))
        assertEquals(Bounds.getNextBounds(64, 127), Bounds(64, 95, 96, 127))
        assertEquals(Bounds.getNextBounds(32, 63), Bounds(32, 47, 48, 63))
        assertEquals(Bounds.getNextBounds(32, 47), Bounds(32, 39, 40, 47))
    }

    @Test
    fun `should parse path component`() {
        assertEquals(charToSide('F'), Side.Left)
        assertEquals(charToSide('B'), Side.Right)
        assertEquals(charToSide('L'), Side.Left)
        assertEquals(charToSide('R'), Side.Right)
        assertFailsWith<Error> {
            charToSide('t')
        }
    }

    @Test
    fun `should perform binary search`() {

        assertEquals(binarySearch(0, 127, "FBFBBFF"), 44)
        assertEquals(binarySearch(0, 127, "BFFFBBF"), 70)
        assertEquals(binarySearch(0, 127, "FFFBBBF"), 14)
        assertEquals(binarySearch(0, 127, "BBFFBBF"), 102)

        assertEquals(binarySearch(0, 7, "RRR"), 7)
        assertEquals(binarySearch(0, 7, "RLL"), 4)
        assertEquals(binarySearch(0, 7, "RLR"), 5)

    }

    @Test
    fun `should return the seat ID`() {
        assertEquals(getSeat("FBFBBFFRLR").getSeatId(), 357)
        assertEquals(getSeat("BFFFBBFRRR").getSeatId(), 567)
        assertEquals(getSeat("FFFBBBFRRR").getSeatId(), 119)
        assertEquals(getSeat("BBFFBBFRLL").getSeatId(), 820)
    }

    @Test
    fun `should run part1`() {
        val lines = listOf(
            "FBFBBFFRLR",
            "BFFFBBFRRR",
            "FFFBBBFRRR",
            "BBFFBBFRLL"
        )
        assertEquals(runPart1(lines), 820)
    }
}