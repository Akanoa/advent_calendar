# Advent of Code

Ce dépôt répertorie l'ensemble de mes participations au challenge [Advent of Code](https://adventofcode.com/).

Il s'agit comme son nom l'indique d'un calendrier de l'avent mais pour développeurs. Chaque jour jusqu'au sapin un nouveau problème à résoudre 🎁.

Le choix du langage est complètement libre. J'essaierai de changer de langage d'une année à l'autre.

## Mes objectifs

- Apprendre  les concepts d'un langage qui m'est inconnu
- Me confronter des problèmes algorithmiques
- M'amuser

## Organisation du projet

Il existe un dossier pour chaque année et chaque année est découpé en dossiers représentant une journée.

Chaque journée est un problème différent.

## Twitch

Je ferai exclusivement le challenge sur [Twitch](https://www.twitch.tv/akanoa), donc si vous avez envie de passer vous êtes les bienvenus.

## Années précédentes

- 2020 : [Kotlin](./2020)
- 2019 : [Rust](./2019)
