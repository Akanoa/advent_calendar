const std = @import("std");


fn part1(path: [] const u8) !u32 {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;
    var sum : u32 = 0;

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {

        sum += Round.new(try MyPlay.from_char(line[2..3]), try OpponentPlay.from_char(line[0..1])).count_points();

    }

    return sum;
}


fn part2(path: [] const u8) !u32 {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;
    var sum : u32 = 0;

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {

        sum += RoundBiased.new(try Result.from_char(line[2..3]), try OpponentPlay.from_char(line[0..1])).count_points();

    }

    return sum;
}


const Result = enum(u32) {
    Win = 6,
    Draw = 3,
    Lose = 0,

    fn from_char(char: [] const u8) !Result {

        if (@as(u8, char[0]) == 'X') {
            return Result.Lose;
        } else if (@as(u8, char[0]) == 'Y') {
            return Result.Draw;
        } else if (@as(u8, char[0]) == 'Z') {
            return Result.Win;
        } else {
            unreachable;
        }
    }
};

const MyPlay = enum(u32) {
    Rock = 1,
    Paper = 2,
    Scissor = 3,

    fn from_char(char: [] const u8) !MyPlay {

        if (@as(u8, char[0]) == 'X') {
            return MyPlay.Rock;
        } else if (@as(u8, char[0]) == 'Y') {
            return MyPlay.Paper;
        } else if (@as(u8, char[0]) == 'Z') {
            return MyPlay.Scissor;
        } else {
            std.debug.print("\n{s}\n", .{char});
            unreachable;
        }
    }

    fn display(self: MyPlay) void {

        switch(self) {
            MyPlay.Rock => std.debug.print("\nrock", .{}),
            MyPlay.Paper => std.debug.print("\npaper", .{}),
            MyPlay.Scissor => std.debug.print("\nscissor", .{}),
        }

    }
};

const OpponentPlay = enum {
    Rock, 
    Paper,
    Scissor,

    fn from_char(char: [] const u8) !OpponentPlay {

        if (@as(u8, char[0]) == 'A') {
            return OpponentPlay.Rock;
        } else if (@as(u8, char[0]) == 'B') {
            return OpponentPlay.Paper;
        } else if (@as(u8, char[0]) == 'C') {
            return OpponentPlay.Scissor;
        } else {
            unreachable;
        }
    }
};


const RoundBiased = struct {
    play: OpponentPlay,
    result: Result,

    fn new(result: Result, play: OpponentPlay) RoundBiased {
        return RoundBiased {
            .play = play,
            .result = result
        };
    }

    fn fight(self: RoundBiased) MyPlay {
        switch(self.play) {
            OpponentPlay.Rock => switch(self.result) {
                Result.Lose => return MyPlay.Scissor,
                Result.Draw => return MyPlay.Rock,
                Result.Win => return MyPlay.Paper,
            },
            OpponentPlay.Paper => switch(self.result) {
                Result.Lose => return MyPlay.Rock,
                Result.Draw => return MyPlay.Paper,
                Result.Win => return MyPlay.Scissor,
            },
            OpponentPlay.Scissor => switch(self.result) {
                Result.Lose => return MyPlay.Paper,
                Result.Draw => return MyPlay.Scissor,
                Result.Win => return MyPlay.Rock,
            },
        }
    }

    fn count_points(self: RoundBiased) u32 {
        var draw_to_play = self.fight();
        var points_from_round = @enumToInt(self.result);
        var points_from_draw = @enumToInt(draw_to_play);
        return points_from_round + points_from_draw;
    } 
};


const Round = struct {
    my_play: MyPlay,
    opponent_play: OpponentPlay,

    fn new(my_play: MyPlay, opponent_play: OpponentPlay) Round {
        return Round {
            .my_play = my_play,
            .opponent_play = opponent_play
        };
    }

    fn fight(self: Round) Result {
        switch(self.my_play) {
            MyPlay.Rock => switch(self.opponent_play) {
                OpponentPlay.Rock => return Result.Draw,
                OpponentPlay.Scissor => return Result.Win,
                OpponentPlay.Paper => return Result.Lose,
            },
            MyPlay.Paper => switch(self.opponent_play) {
                OpponentPlay.Paper => return Result.Draw,
                OpponentPlay.Rock => return Result.Win,
                OpponentPlay.Scissor => return Result.Lose,
            },
            MyPlay.Scissor => switch(self.opponent_play) {
                OpponentPlay.Scissor => return Result.Draw,
                OpponentPlay.Paper => return Result.Win,
                OpponentPlay.Rock => return Result.Lose,
            },
        }
    }

    fn count_points(self: Round) u32 {
        var result_fight = self.fight();
        var points_from_round = @enumToInt(result_fight);
        var points_from_draw = @enumToInt(self.my_play);
        return points_from_round + points_from_draw;
    } 
};

pub fn main() !void {
    
    var result1 = try part1("part1.txt");
    std.debug.print("result part 1: {d}", .{result1});

    var result2 = try part2("part1.txt");
    std.debug.print("result part 2: {d}", .{result2});
}

test "my_play::from_char" {
    try std.testing.expectEqual(try MyPlay.from_char("X"), MyPlay.Rock);
    try std.testing.expectEqual(try MyPlay.from_char("Y"), MyPlay.Paper);
    try std.testing.expectEqual(try MyPlay.from_char("Z"), MyPlay.Scissor);
}

test "opponent_play::from_char" {
    try std.testing.expectEqual(try OpponentPlay.from_char("A"), OpponentPlay.Rock);
    try std.testing.expectEqual(try OpponentPlay.from_char("B"), OpponentPlay.Paper);
    try std.testing.expectEqual(try OpponentPlay.from_char("C"), OpponentPlay.Scissor);
}

test "result::from_char" {
    try std.testing.expectEqual(try Result.from_char("X"), Result.Lose);
    try std.testing.expectEqual(try Result.from_char("Y"), Result.Draw);
    try std.testing.expectEqual(try Result.from_char("Z"), Result.Win);
}

test "first strategy" {
    var round1 = Round.new(try MyPlay.from_char("Y"), try OpponentPlay.from_char("A")).count_points();
    try std.testing.expectEqual(round1, 8);

    var round2 = Round.new(try MyPlay.from_char("X"), try OpponentPlay.from_char("B")).count_points();
    try std.testing.expectEqual(round2, 1);

    var round3 = Round.new(try MyPlay.from_char("Z"), try OpponentPlay.from_char("C")).count_points();
    try std.testing.expectEqual(round3, 6);
}

test "fight" {

    try std.testing.expectEqual(Round.new(MyPlay.Rock, OpponentPlay.Rock).fight(), Result.Draw);
    try std.testing.expectEqual(Round.new(MyPlay.Paper, OpponentPlay.Scissor).fight(), Result.Lose);
    try std.testing.expectEqual(Round.new(MyPlay.Scissor, OpponentPlay.Paper).fight(), Result.Win);
}

test "demo1" {
    try std.testing.expectEqual(try part1("demo1.txt"), 15);
}

test "demo2" {
    try std.testing.expectEqual(try part2("demo1.txt"), 12);
}