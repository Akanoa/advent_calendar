const std = @import("std");
const ArrayList = std.ArrayList;

const GroupIndexes = struct {
    idx1: usize,
    idx2: usize,
    idx3: usize,
};

const Group = struct {
    backpack1: [] const u8,
    backpack2: [] const u8,
    backpack3: [] const u8,

    fn findBadge(self: Group) u8 {

        for(self.backpack1) |b1_item| {

            for(self.backpack2) |b2_item| {

                for(self.backpack3) |b3_item| {
                
                    if (b1_item == b2_item and b2_item == b3_item) {
                        return getPriority(b1_item);
                    }
                
                }

            }
        }

        unreachable;
    }

    fn new(b1 : [] const u8, b2: [] const u8, b3: [] const u8) Group {

        return Group {
            .backpack1 = b1,
            .backpack2 = b2,
            .backpack3 = b3,
        };
    }

    fn display(self: Group) void {
        std.debug.print("\n---\nb1: {s}, b2: {s}, b3: {s}\n---\n", .{self.backpack1, self.backpack2, self.backpack3});
    }
};


fn getPriority(item: u8) u8 {
    return switch(item) {
        65 ... 90 => item - 64 + 26,
        97 ... 122 => item - 96,
        else => unreachable
    };
}


const BackPack = struct {
    compartment1: [] const u8,
    compartment2: [] const u8,

    fn new(content: [] const u8) BackPack {


        //std.debug.print("\nlen: {d}\n", .{content.len});

        var compartment1 = content[0..content.len / 2];
        var compartment2 = content[content.len / 2..];

        //std.debug.print("\nc1 : {s}, c2: {s}\n", .{compartment1, compartment2});


        return BackPack {
            .compartment1 = compartment1,
            .compartment2 = compartment2 
        };
    }

    fn findDuplicate(self: BackPack) u8 {

        for(self.compartment1) |c1_item| {

            for(self.compartment2) |c2_item| {
                if (c1_item == c2_item) {
                    return c1_item;
                }
            }
        }

        unreachable;
    }

    fn getDuplicateItemPriority(self: BackPack) u8 {
        var duplicateItem = self.findDuplicate();

        return getPriority(duplicateItem);

    }

    fn eql(self: BackPack, other: BackPack) bool {
        if ((!std.mem.startsWith(u8, self.compartment1, other.compartment1)) 
            or (!std.mem.startsWith(u8, self.compartment2, other.compartment2))) {
            return false;
        }
        return true;
    }
};


fn part1(path: [] const u8) !u32 {

    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;
    var sum : u32 = 0;

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {

        sum += BackPack.new(line).getDuplicateItemPriority();

    }

    return sum;
}

fn part2(path: [] const u8) !u32 {

    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;
    var sum : u32 = 0;

    var index: usize = 0;
    var line_counter : usize = 0;
    var groupIndex = GroupIndexes {.idx1 = 0, .idx2 = 0, .idx3 = 0};

    outer : while(true) {

        while(true) {

            const byte = in_stream.readByte() catch |err| switch (err) {
                error.EndOfStream => {

                    break :outer;
                },
                else => |e| return e,
            };

            if (byte == '\n') {

                switch(line_counter) {
                    0 => groupIndex.idx1 = index,
                    1 => groupIndex.idx2 = index,
                    2 => groupIndex.idx3 = index,
                    else => unreachable
                }

                line_counter += 1;
            }

            if (line_counter == 3) {
                index = 0;
                line_counter = 0;
                sum += Group.new(
                    buf[0..groupIndex.idx1], 
                    buf[groupIndex.idx1+1..groupIndex.idx2], 
                    buf[groupIndex.idx2+1..groupIndex.idx3]
                ).findBadge();
            }

            buf[index] = byte;
            index += 1;

        }
    }


    //std.debug.print("\n---\nb1: {s}, b2: {s}, b3: {s}\n---\n", .{buf[0..groupIndex.idx1], buf[groupIndex.idx1+1..groupIndex.idx2], buf[groupIndex.idx2+1..groupIndex.idx3]});

    return sum;
} 

pub fn main() !void {
    var result1 = try part1("part1.txt");
    std.debug.print("part1 result : {d}", .{result1});


    var result2 = try part2("part1.txt");
    std.debug.print("part1 result : {d}", .{result2});

}

test "demo 1" {
    var result = try part1("demo.txt");
    try std.testing.expectEqual(result, 157);
}

test "demo 2" {
    var result = try part2("demo.txt");
    try std.testing.expectEqual(result, 70);
}

test "backpack::new" {
    var result1 = BackPack.new("vJrwpWtwJgWrhcsFMMfFFhFp");
    try std.testing.expect(result1.eql(BackPack {
        .compartment1 = "vJrwpWtwJgWr",
        .compartment2 = "hcsFMMfFFhFp"
    }));

    var result2 = BackPack.new("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL");
    try std.testing.expect(result2.eql(BackPack {
        .compartment1 = "jqHRNqRjqzjGDLGL",
        .compartment2 = "rsFMfFZSrLrFZsSL"
    }));
}

test "backpack::findDuplicate" {
    var result1 = BackPack.new("vJrwpWtwJgWrhcsFMMfFFhFp").findDuplicate();
    try std.testing.expectEqual(result1, 'p');

    var result2 = BackPack.new("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL").findDuplicate();
    try std.testing.expectEqual(result2, 'L');
}

test "backpack::getDuplicateItemPriority" {
    var result1 = BackPack.new("vJrwpWtwJgWrhcsFMMfFFhFp").getDuplicateItemPriority();
    try std.testing.expectEqual(result1, 16);

    var result2 = BackPack.new("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL").getDuplicateItemPriority();
    try std.testing.expectEqual(result2, 38);

    var result3 = BackPack.new("PmmdzqPrVvPwwTWBwg").getDuplicateItemPriority();
    try std.testing.expectEqual(result3, 42);

    var result4 = BackPack.new("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn").getDuplicateItemPriority();
    try std.testing.expectEqual(result4, 22);

    var result5 = BackPack.new("ttgJtRGJQctTZtZT").getDuplicateItemPriority();
    try std.testing.expectEqual(result5, 20);

    var result6 = BackPack.new("CrZsJsPPZsGzwwsLwLmpwMDw").getDuplicateItemPriority();
    try std.testing.expectEqual(result6, 19);
}

test "group::findBadge" {
    var result1 = Group.new("vJrwpWtwJgWrhcsFMMfFFhFp", "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", "PmmdzqPrVvPwwTWBwg").findBadge();
    try std.testing.expectEqual(result1, 18);

    var result2 = Group.new("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", "ttgJtRGJQctTZtZT", "CrZsJsPPZsGzwwsLwLmpwMDw").findBadge();
    try std.testing.expectEqual(result2, 52);
}