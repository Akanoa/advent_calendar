const std = @import("std");
const ArrayList = std.ArrayList;
const test_allocator = std.heap.page_allocator;
const ascii = std.ascii;

const io = std.io;


const OneByteReadReader = struct {
    str: []const u8,
    curr: usize,

    const Error = error{NoError};
    const Self = @This();
    const Reader = io.Reader(*Self, Error, read);

    fn init(str: []const u8) Self {
        return Self{
            .str = str,
            .curr = 0,
        };
    }

    fn read(self: *Self, dest: []u8) Error!usize {
        if (self.str.len <= self.curr or dest.len == 0)
            return 0;

        dest[0] = self.str[self.curr];
        self.curr += 1;
        return 1;
    }

    fn reader(self: *Self) Reader {
        return .{ .context = self };
    }
};

const Command = struct {
    nb: u8,
    from: u8,
    to: u8,

    fn new(nb: u8, from: u8, to: u8) Command {
        return Command {
            .nb = nb,
            .from = from,
            .to = to
        };
    }

    fn from_string(line: [] const u8) !Command {

        var buf : [100]u8 = undefined;

        //move 10 from 12 to 25
        //^

        var one_byte_stream = OneByteReadReader.init(line[5..]);
        var buf_reader = io.bufferedReader(one_byte_stream.reader());

        //move 10 from 12 to 25
        //     ^


        var stream = buf_reader.reader();

        var result1 = try stream.readUntilDelimiter(&buf, ' ');

        var nb : u8 = try std.fmt.parseInt(u8, result1, 10);

        //move 10 from 12 to 25
        //       ^

        try stream.skipBytes(5, .{});

        //move 10 from 12 to 25
        //             ^

        var result2 = try stream.readUntilDelimiter(&buf, ' ');
        var from : u8 = try std.fmt.parseInt(u8, result2, 10);

        //move 10 from 12 to 25
        //               ^

        try stream.skipBytes(3, .{});

        //move 10 from 12 to 25
        //                   ^

        var bytes_read = try stream.readAll(&buf);
        var to : u8 = try std.fmt.parseInt(u8, buf[0..bytes_read], 10);

        return Command.new(nb,from,to);
    }

    fn eql(self: Command, other: Command) bool {
        return self.nb == other.nb and self.from == other.from and self.to == other.to;
    }

    fn display(self: Command) void {
        std.debug.print("move {} from {} to {}\n", .{self.nb, self.from, self.to});
    }
};


const Crate = struct {
    name: [] const u8,
    fn new(name: [] const u8) Crate {
        return Crate {
            .name = name
        };
    }

    fn display(self: Crate) void {
        std.debug.print("[{s}]\n", .{self.name});
    }
};

const Stack = struct {
    inner : std.ArrayList(Crate),
    number: u8,

    fn new(number: u8) Stack {
        return Stack {
            .number = number,
            .inner = ArrayList(Crate).init(test_allocator)
        };
    }

    fn getLen(self: Stack) usize {
        return self.inner.items.len;
    }

    fn display(self: Stack) void {

        std.debug.print("stack {d} : ", .{self.number});
        for (self.inner.items) |crate| {
            std.debug.print("[{s}], ", .{crate.name});
        }
        std.debug.print("\n", .{});
    }

    fn deInit(self: *Stack) void {
        self.inner.deinit();
    }

    fn push(self : *Stack ,crate: Crate) !void {
        try self.inner.append(crate);
    }

    fn pop(self: *Stack) ?Crate {
        return self.inner.popOrNull();
    }
};

const WhareHouse = struct {
    stacks : ArrayList(*Stack),

    fn new() WhareHouse {
        return WhareHouse {
            .stacks = ArrayList(*Stack).init(test_allocator)
        };
    }

    fn deInit(self: *WhareHouse) void {
        self.stacks.deinit();
    }

    fn display(self: *WhareHouse) void {

        std.debug.print("\n", .{});
        for (self.stacks.items) |stack| {
 
            stack.display();
        }

    }


    fn add(self: *WhareHouse, stack: *Stack) !void {
        return self.stacks.append(stack);
    }

    fn move(self: WhareHouse, command: Command) !void {

        //command.display();

        var index: u8 = 0;
        while(index < command.nb) {

            var crate = (self.stacks.items[command.from - 1]).pop().?;

            var to_stack = self.stacks.items[command.to - 1];

            try to_stack.push(crate);

            index +=1;
        }

    }

    fn moveMulti(self: WhareHouse, command: Command) ! void {

        var stack_src = self.stacks.items[command.from - 1];
        var stack_dest = self.stacks.items[command.to - 1];
        

        var index_to_remove = stack_src.inner.items.len - command.nb;
        var index : u8 = 0;


        while(index < command.nb) {
            
            var crate = stack_src.inner.orderedRemove(index_to_remove);
            try stack_dest.push(crate);
            
            index += 1;
        }
    }

    fn top(self: *WhareHouse) !void {
        
        std.debug.print("top: ", .{});
        var index: usize = 0;
        for (self.stacks.items) |stack| {
 
            var crate = stack.pop().?;
            std.debug.print("{s}", .{crate.name});
            index += 1;

        }
        std.debug.print("\n", .{});
    } 
};

fn part1(path: [] const u8, wharehouse: *WhareHouse) !void {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {

        var command = try Command.from_string(line);
        try wharehouse.move(command);

    }
    try wharehouse.top();
}

fn part2(path: [] const u8, wharehouse: *WhareHouse) !void {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {

        var command = try Command.from_string(line);
        try wharehouse.moveMulti(command);

    }
    try wharehouse.top();
}

fn main2() !void {
        var stack1 = Stack.new(1);
    try stack1.push(Crate.new("Z"));
    try stack1.push(Crate.new("J"));
    try stack1.push(Crate.new("G"));
    defer stack1.deInit();

    var stack2 = Stack.new(2);
    try stack2.push(Crate.new("Q"));
    try stack2.push(Crate.new("L"));
    try stack2.push(Crate.new("R"));
    try stack2.push(Crate.new("P"));
    try stack2.push(Crate.new("W"));
    try stack2.push(Crate.new("F"));
    try stack2.push(Crate.new("V"));
    try stack2.push(Crate.new("C"));
    defer stack2.deInit();

    var stack3 = Stack.new(3);
    try stack3.push(Crate.new("F"));
    try stack3.push(Crate.new("P"));
    try stack3.push(Crate.new("M"));
    try stack3.push(Crate.new("C"));
    try stack3.push(Crate.new("L"));
    try stack3.push(Crate.new("G"));
    try stack3.push(Crate.new("R"));
    defer stack3.deInit();

    var stack4 = Stack.new(4);
    try stack4.push(Crate.new("L"));
    try stack4.push(Crate.new("F"));
    try stack4.push(Crate.new("B"));
    try stack4.push(Crate.new("W"));
    try stack4.push(Crate.new("P"));
    try stack4.push(Crate.new("H"));
    try stack4.push(Crate.new("M"));
    defer stack4.deInit();

    var stack5 = Stack.new(5);
    try stack5.push(Crate.new("G"));
    try stack5.push(Crate.new("C"));
    try stack5.push(Crate.new("F"));
    try stack5.push(Crate.new("S"));
    try stack5.push(Crate.new("V"));
    try stack5.push(Crate.new("Q"));
    defer stack5.deInit();

    var stack6 = Stack.new(6);
    try stack6.push(Crate.new("W"));
    try stack6.push(Crate.new("H"));
    try stack6.push(Crate.new("J"));
    try stack6.push(Crate.new("Z"));
    try stack6.push(Crate.new("M"));
    try stack6.push(Crate.new("Q"));
    try stack6.push(Crate.new("T"));
    try stack6.push(Crate.new("L"));
    defer stack6.deInit();

    var stack7 = Stack.new(7);
    try stack7.push(Crate.new("H"));
    try stack7.push(Crate.new("F"));
    try stack7.push(Crate.new("S"));
    try stack7.push(Crate.new("B"));
    try stack7.push(Crate.new("V"));
    defer stack7.deInit();

    
    var stack8 = Stack.new(8);
    try stack8.push(Crate.new("F"));
    try stack8.push(Crate.new("J"));
    try stack8.push(Crate.new("Z"));
    try stack8.push(Crate.new("S"));
    defer stack8.deInit();

    var stack9 = Stack.new(9);
    try stack9.push(Crate.new("M"));
    try stack9.push(Crate.new("C"));
    try stack9.push(Crate.new("D"));
    try stack9.push(Crate.new("P"));
    try stack9.push(Crate.new("F"));
    try stack9.push(Crate.new("H"));
    try stack9.push(Crate.new("B"));
    try stack9.push(Crate.new("T"));
    defer stack9.deInit();

    var wharehouse = WhareHouse.new();
    try wharehouse.add(&stack1);
    try wharehouse.add(&stack2);
    try wharehouse.add(&stack3);
    try wharehouse.add(&stack4);
    try wharehouse.add(&stack5);
    try wharehouse.add(&stack6);
    try wharehouse.add(&stack7);
    try wharehouse.add(&stack8);
    try wharehouse.add(&stack9);
    defer wharehouse.deInit();

    try part2("part1.txt", &wharehouse);
}

pub fn main() !void {

    var stack1 = Stack.new(1);
    try stack1.push(Crate.new("Z"));
    try stack1.push(Crate.new("J"));
    try stack1.push(Crate.new("G"));
    defer stack1.deInit();

    var stack2 = Stack.new(2);
    try stack2.push(Crate.new("Q"));
    try stack2.push(Crate.new("L"));
    try stack2.push(Crate.new("R"));
    try stack2.push(Crate.new("P"));
    try stack2.push(Crate.new("W"));
    try stack2.push(Crate.new("F"));
    try stack2.push(Crate.new("V"));
    try stack2.push(Crate.new("C"));
    defer stack2.deInit();

    var stack3 = Stack.new(3);
    try stack3.push(Crate.new("F"));
    try stack3.push(Crate.new("P"));
    try stack3.push(Crate.new("M"));
    try stack3.push(Crate.new("C"));
    try stack3.push(Crate.new("L"));
    try stack3.push(Crate.new("G"));
    try stack3.push(Crate.new("R"));
    defer stack3.deInit();

    var stack4 = Stack.new(4);
    try stack4.push(Crate.new("L"));
    try stack4.push(Crate.new("F"));
    try stack4.push(Crate.new("B"));
    try stack4.push(Crate.new("W"));
    try stack4.push(Crate.new("P"));
    try stack4.push(Crate.new("H"));
    try stack4.push(Crate.new("M"));
    defer stack4.deInit();

    var stack5 = Stack.new(5);
    try stack5.push(Crate.new("G"));
    try stack5.push(Crate.new("C"));
    try stack5.push(Crate.new("F"));
    try stack5.push(Crate.new("S"));
    try stack5.push(Crate.new("V"));
    try stack5.push(Crate.new("Q"));
    defer stack5.deInit();

    var stack6 = Stack.new(6);
    try stack6.push(Crate.new("W"));
    try stack6.push(Crate.new("H"));
    try stack6.push(Crate.new("J"));
    try stack6.push(Crate.new("Z"));
    try stack6.push(Crate.new("M"));
    try stack6.push(Crate.new("Q"));
    try stack6.push(Crate.new("T"));
    try stack6.push(Crate.new("L"));
    defer stack6.deInit();

    var stack7 = Stack.new(7);
    try stack7.push(Crate.new("H"));
    try stack7.push(Crate.new("F"));
    try stack7.push(Crate.new("S"));
    try stack7.push(Crate.new("B"));
    try stack7.push(Crate.new("V"));
    defer stack7.deInit();

    
    var stack8 = Stack.new(8);
    try stack8.push(Crate.new("F"));
    try stack8.push(Crate.new("J"));
    try stack8.push(Crate.new("Z"));
    try stack8.push(Crate.new("S"));
    defer stack8.deInit();

    var stack9 = Stack.new(9);
    try stack9.push(Crate.new("M"));
    try stack9.push(Crate.new("C"));
    try stack9.push(Crate.new("D"));
    try stack9.push(Crate.new("P"));
    try stack9.push(Crate.new("F"));
    try stack9.push(Crate.new("H"));
    try stack9.push(Crate.new("B"));
    try stack9.push(Crate.new("T"));
    defer stack9.deInit();

    var wharehouse = WhareHouse.new();
    try wharehouse.add(&stack1);
    try wharehouse.add(&stack2);
    try wharehouse.add(&stack3);
    try wharehouse.add(&stack4);
    try wharehouse.add(&stack5);
    try wharehouse.add(&stack6);
    try wharehouse.add(&stack7);
    try wharehouse.add(&stack8);
    try wharehouse.add(&stack9);
    defer wharehouse.deInit();

    try part1("part1.txt", &wharehouse);
    try main2();

}

test "Command::from_string" {
    try std.testing.expect((try Command.from_string("move 1 from 2 to 1")).eql(Command {.nb = 1, .from = 2, .to = 1}));
    try std.testing.expect((try Command.from_string("move 3 from 1 to 3")).eql(Command {.nb = 3, .from = 1, .to = 3}));
    try std.testing.expect((try Command.from_string("move 2 from 2 to 1")).eql(Command {.nb = 2, .from = 2, .to = 1}));
    try std.testing.expect((try Command.from_string("move 1 from 1 to 2")).eql(Command {.nb = 1, .from = 1, .to = 2}));
    try std.testing.expect((try Command.from_string("move 14 from 15 to 12")).eql(Command {.nb = 14, .from = 15, .to = 12}));
}

// test "WhareHouse::move" {

//     var stack1 = Stack.new(1);
//     try stack1.push(Crate.new("Z"));
//     try stack1.push(Crate.new("N"));
//     defer stack1.deInit();

//     var stack2 = Stack.new(2);
//     try stack2.push(Crate.new("M"));
//     try stack2.push(Crate.new("C"));
//     try stack2.push(Crate.new("D"));
//     defer stack2.deInit();

//     var stack3 = Stack.new(3);
//     try stack3.push(Crate.new("P"));
//     defer stack3.deInit();

//     var wharehouse = WhareHouse.new();
//     try wharehouse.add(&stack1);
//     try wharehouse.add(&stack2);
//     try wharehouse.add(&stack3);
//     defer wharehouse.deInit();



//     var command1 = try Command.from_string("move 1 from 2 to 1");
//     var command2 = try Command.from_string("move 3 from 1 to 3");
//     var command3 = try Command.from_string("move 2 from 2 to 1");
//     var command4 = try Command.from_string("move 1 from 1 to 2");

//     wharehouse.display();


//     try wharehouse.moveMulti(command1);
//     wharehouse.display();

//     try wharehouse.moveMulti(command2);
//     wharehouse.display();

//     try wharehouse.moveMulti(command3);
//     wharehouse.display();

//     try wharehouse.moveMulti(command4);
//     wharehouse.display();

//     try wharehouse.top();


// }

test "part1" {

    var stack1 = Stack.new(1);
    try stack1.push(Crate.new("Z"));
    try stack1.push(Crate.new("N"));
    defer stack1.deInit();

    var stack2 = Stack.new(2);
    try stack2.push(Crate.new("M"));
    try stack2.push(Crate.new("C"));
    try stack2.push(Crate.new("D"));
    defer stack2.deInit();

    var stack3 = Stack.new(3);
    try stack3.push(Crate.new("P"));
    defer stack3.deInit();

    var wharehouse = WhareHouse.new();
    try wharehouse.add(&stack1);
    try wharehouse.add(&stack2);
    try wharehouse.add(&stack3);
    defer wharehouse.deInit();

    try part1("demo.txt", &wharehouse);
}

test "part2" {

    var stack1 = Stack.new(1);
    try stack1.push(Crate.new("Z"));
    try stack1.push(Crate.new("N"));
    defer stack1.deInit();

    var stack2 = Stack.new(2);
    try stack2.push(Crate.new("M"));
    try stack2.push(Crate.new("C"));
    try stack2.push(Crate.new("D"));
    defer stack2.deInit();

    var stack3 = Stack.new(3);
    try stack3.push(Crate.new("P"));
    defer stack3.deInit();

    var wharehouse = WhareHouse.new();
    try wharehouse.add(&stack1);
    try wharehouse.add(&stack2);
    try wharehouse.add(&stack3);
    defer wharehouse.deInit();

    try part2("demo.txt", &wharehouse);
}