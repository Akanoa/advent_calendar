const std = @import("std");
const io = std.io;


const OneByteReadReader = struct {
    str: []const u8,
    curr: usize,

    const Error = error{NoError};
    const Self = @This();
    const Reader = io.Reader(*Self, Error, read);

    fn init(str: []const u8) Self {
        return Self{
            .str = str,
            .curr = 0,
        };
    }

    fn read(self: *Self, dest: []u8) Error!usize {
        if (self.str.len <= self.curr or dest.len == 0)
            return 0;

        dest[0] = self.str[self.curr];
        self.curr += 1;
        return 1;
    }

    fn reader(self: *Self) Reader {
        return .{ .context = self };
    }
};


const Range = struct {
    bound1: u32,
    bound2: u32,

    fn from_string(line : [] const u8) !Range {


        var buf : [10]u8 = undefined;

        var one_byte_stream = OneByteReadReader.init(line);
        var buf_reader = io.bufferedReader(one_byte_stream.reader());


        var stream = buf_reader.reader();

        var result = try stream.readUntilDelimiter(&buf, '-');
        var bound1 : u32 = try std.fmt.parseInt(u32, result, 10);

        var read_bytes = try stream.readAll(&buf);
        var bound2 : u32 = try std.fmt.parseInt(u32, buf[0..read_bytes], 10);


        return Range {
            .bound1 = bound1,
            .bound2 = bound2
        };
    }

    fn eql(self: Range, other: Range) bool {
        return self.bound1 == other.bound1 and self.bound2 == other.bound2;
    }

    fn display(self: Range) void {
        std.debug.print("Range: b1: {d}, b2: {}\n", .{self.bound1, self.bound2});
    }
};

const Pair = struct {
    range1 : Range,
    range2 : Range,

    fn from_string(line: [] const u8) !Pair {

        var buf : [20]u8 = undefined;

        var one_byte_stream = OneByteReadReader.init(line);
        var buf_reader = io.bufferedReader(one_byte_stream.reader());


        var stream = buf_reader.reader();

        var result = try stream.readUntilDelimiter(&buf, ',');


        var range1 = try Range.from_string(result);

        var read_bytes = try stream.readAll(&buf);

        var range2 = try Range.from_string(buf[0..read_bytes]);
        
        return Pair {
            .range1 = range1,
            .range2 = range2
        };

    }

    fn overlap(self: Pair) bool {
        
        var l1 = self.range1.bound2 - self.range1.bound1;
        var l2 = self.range2.bound2 - self.range2.bound1;


        if (l1 >= l2) {

            return self.range2.bound1 >= self.range1.bound1 and self.range2.bound2 <= self.range1.bound2;
        } else {

            return self.range1.bound1 >= self.range2.bound1 and self.range1.bound2 <= self.range2.bound2;
        }
    }

    fn intersect(self: Pair) bool {

        if (self.range1.bound1 < self.range2.bound1) {
            return self.range1.bound2 >= self.range2.bound1;
        } else if (self.range1.bound1 > self.range2.bound1) {
            return self.range2.bound2 >= self.range1.bound1;
        } else {
            return true;
        }


    }

    fn eql(self: Pair, other: Pair) bool {
        return self.range1.eql(other.range1) and self.range2.eql(other.range2);
    }
};

fn part1(path: [] const u8) !u32 {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;
    var sum : u32 = 0;

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {

        if ((try Pair.from_string(line)).overlap()) {
            sum += 1;
        } 

    }

    return sum;
}

fn part2(path: [] const u8) !u32 {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;
    var sum : u32 = 0;

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {

        if ((try Pair.from_string(line)).intersect()) {
            sum += 1;
        } 

    }

    return sum;
}

pub fn main() !void {
    var result1 = try part1("part1.txt");
    std.debug.print("Result part1 : {d} section overlaping\n", .{result1});

    var result2 = try part2("part1.txt");
    std.debug.print("Result part2 : {d} section intersecting", .{result2});
}

test "range::from_string" {
    try std.testing.expect((try Range.from_string("2-4")).eql(Range { .bound1= 2, .bound2 = 4}));
    try std.testing.expect((try Range.from_string("6-8")).eql(Range { .bound1= 6, .bound2 = 8}));
    try std.testing.expect((try Range.from_string("6-6")).eql(Range { .bound1= 6, .bound2 = 6}));
    try std.testing.expect((try Range.from_string("68-678")).eql(Range { .bound1= 68, .bound2 = 678}));
}

test "pair::from_string" {
    try std.testing.expect((try Pair.from_string("2-4,6-8")).eql(Pair {
        .range1 = try Range.from_string("2-4"),
        .range2 = try Range.from_string("6-8")
    }));

    try std.testing.expect((try Pair.from_string("62-90,70-74")).eql(Pair {
        .range1 = try Range.from_string("62-90"),
        .range2 = try Range.from_string("70-74")
    }));
}

test "pair::overlap" {
    try std.testing.expect(!(try Pair.from_string("2-4,6-8")).overlap());
    try std.testing.expect(!(try Pair.from_string("2-3,4-5")).overlap());
    try std.testing.expect(!(try Pair.from_string("5-7,7-9")).overlap());
    try std.testing.expect((try Pair.from_string("2-8,3-7")).overlap());
    try std.testing.expect((try Pair.from_string("6-6,4-6")).overlap());
    try std.testing.expect(!(try Pair.from_string("2-6,4-8")).overlap());
}

test "pair::intersect" {
    try std.testing.expect(!(try Pair.from_string("2-4,6-8")).intersect());
    try std.testing.expect(!(try Pair.from_string("2-3,4-5")).intersect());
    try std.testing.expect((try Pair.from_string("5-7,7-9")).intersect());
    try std.testing.expect((try Pair.from_string("2-8,3-7")).intersect());
    try std.testing.expect((try Pair.from_string("6-6,4-6")).intersect());
    try std.testing.expect((try Pair.from_string("2-6,4-8")).intersect());
}

test "part1" {
    try std.testing.expectEqual(try part1("demo.txt"), 2);
}

test "part2" {
    try std.testing.expectEqual(try part2("demo.txt"), 4);
}