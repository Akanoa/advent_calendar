with open("part1.txt") as file:
    
    elves = []
    supplies = []
    
    for line in file.readlines():
        if line == "\n": 
            elves.append(supplies.copy())
            supplies.clear()
            continue
        val = int(line)
        supplies.append(val)
        
    e = map(lambda x : sum(x), elves)
        
    print(max(list(e)))        
        