const std = @import("std");
const assert = @import("std").debug.assert;

const Result = struct {
    max_value: u32,
    max_index: u32,

    fn print(self: Result) void {
        std.debug.print("value: {d}, index: {d}", .{self.max_value, self.max_index});
    }
};

fn part1(path: [] const u8) !Result {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;
    var current_value: u32 = 0;
    var current_index: u32 = 1;
    var max_index: u32 = 1;
    var max_value: u32 = 0;
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        if (line.len == 0) {

            if (max_value < current_value) {
                max_value = current_value;
                max_index = current_index;
            }
            current_index += 1;
            current_value = 0;

            continue;
        }
        var supply = try std.fmt.parseInt(u32, line, 10);
        current_value += supply;
    }

    return Result {.max_index = max_index, .max_value = max_value};
}

fn part2(path: [] const u8) !u32 {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [1024]u8 = undefined;
    var current_value: u32 = 0;

    var first: u32 = 0;
    var second: u32 = 0;
    var third: u32 = 0;


    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        if (line.len == 0) {

            if (first < current_value) {
                second = first;
                third = second;
                first = current_value;
            } else if (second < current_value) {
                third = second;
                second = current_value;
            }
            else if (third < current_value) {
                third = current_value;
            }

            current_value = 0;

            continue;
        }
        var supply = try std.fmt.parseInt(u32, line, 10);
        current_value += supply;
    }

    return first + second + third;
}
 
pub fn main() !void {
    var result = try part1("part1.txt");
    result.print();

    var result2 = try part2("part1.txt");
    std.debug.print("\nSum first 3: {d}", .{result2});
}

test "demo1" {
    var result = try part1("demo1.txt");

    try std.testing.expectEqual(result, Result {.max_value = 24000, .max_index = 4});
    result.print();
}

test "demo2" {
    var result = try part2("demo1.txt");

    try std.testing.expectEqual(result, 45000);

}