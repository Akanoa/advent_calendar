const std = @import("std");
const HashMap = std.array_hash_map.AutoArrayHashMap;
const page_allocator = std.heap.page_allocator;

fn part1(path: [] const u8) !usize {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [10000]u8 = undefined;

    var size = try in_stream.readAll(&buf);

    return start_packet(buf[0..size], 4);

}

fn part2(path: [] const u8) !usize {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();
 
    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
 
    var buf: [10000]u8 = undefined;

    var size = try in_stream.readAll(&buf);

    return start_packet(buf[0..size], 14);

}

pub fn main() !void {
    var result1 = try part1("part1.txt");
    std.debug.print("The index of first relevant byte is {}\n", .{result1});

    var result2 = try part2("part1.txt");
    std.debug.print("The index of extended first relevant byte is {}\n", .{result2});
}

fn distinct(data: [] const u8) !bool {
    var map = HashMap(u8, u8).init(page_allocator);

    for(data) |char| {
        if (map.contains(char)) {
            return false;
        }
        try map.put(char, char);
    }

    return true;
}

fn start_packet(line: [] const u8, chunk_size: usize) !usize {

    var index : usize = 0;

    while(index+chunk_size <= line.len) {

        var chunk = line[index..index+chunk_size];

        if (try distinct(chunk)) {
            break;
        }

        index +=1;

    }

    return index+chunk_size;
    
}

test "start_packet" {

    try std.testing.expectEqual(try start_packet("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 4), 7);
    try std.testing.expectEqual(try start_packet("bvwbjplbgvbhsrlpgdmjqwftvncz", 4), 5);
    try std.testing.expectEqual(try start_packet("nppdvjthqldpwncqszvftbrmjlhg", 4), 6);
    try std.testing.expectEqual(try start_packet("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 4), 10);
    try std.testing.expectEqual(try start_packet("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 4), 11);

}

test "start_packet extended" {

    try std.testing.expectEqual(try start_packet("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14), 19);
    try std.testing.expectEqual(try start_packet("bvwbjplbgvbhsrlpgdmjqwftvncz", 14), 23);
    try std.testing.expectEqual(try start_packet("nppdvjthqldpwncqszvftbrmjlhg", 14), 23);
    try std.testing.expectEqual(try start_packet("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 14), 29);
    try std.testing.expectEqual(try start_packet("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 14), 26);

}