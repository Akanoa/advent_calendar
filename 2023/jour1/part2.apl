#!/usr/bin/dyalog -script DYALOG_LINEEDITOR_MODE=1

(⎕NS⍬).(_←enableSALT⊣⎕CY'salt')

]box on

⍝ check un caractère
⍝ renvoie le reste du message non parsé et un status de succès
⍝ ou tout le message et une erreur
check ← {
  ⍝⎕ ← 'debug' ⍵
  to_check ← 1↑⍵
  rest ← 1↓⍵
  ⍺ = to_check : 1 rest ⋄ 0 ⍵
}
⍝ vérifie si le précédent résultat n'est pas une erreur
⍝ si oui renvoie le contenu et propage l'erreur
⍝ sinon exécute le check par rapport au caractère passé à 
tag ← {⍵[1]: ⍺ check⊃⍵[2] ⋄ ⊃(0 ⍵[2])}

⍝ Définition parsers spécifiquess
tag_a ← 'a' tag ⊢
tag_b ← 'b' tag ⊢
tag_e ← 'e' tag ⊢
tag_f ← 'f' tag ⊢
tag_g ← 'g' tag ⊢
tag_h ← 'h' tag ⊢
tag_i ← 'i' tag ⊢
tag_l ← 'l' tag ⊢
tag_n ← 'n' tag ⊢
tag_o ← 'o' tag ⊢
tag_p ← 'p' tag ⊢
tag_r ← 'r' tag ⊢
tag_s ← 's' tag ⊢
tag_t ← 't' tag ⊢
tag_u ← 'u' tag ⊢
tag_v ← 'v' tag ⊢
tag_w ← 'w' tag ⊢
tag_x ← 'x' tag ⊢


⍝ the map_parser takes data, mapped result, size of data consumed
tag_1 ← {{'1' tag ⍵} map_parser (⍵ 1)}
tag_2 ← {{'2' tag ⍵} map_parser (⍵ 2)}
tag_3 ← {{'3' tag ⍵} map_parser (⍵ 3)}
tag_4 ← {{'4' tag ⍵} map_parser (⍵ 4)}
tag_5 ← {{'5' tag ⍵} map_parser (⍵ 5)}
tag_6 ← {{'6' tag ⍵} map_parser (⍵ 6)}
tag_7 ← {{'7' tag ⍵} map_parser (⍵ 7)}
tag_8 ← {{'8' tag ⍵} map_parser (⍵ 8)}
tag_9 ← {{'9' tag ⍵} map_parser (⍵ 9)}

 
tag_one   ← {{ tag_e tag_n tag_o              ⍵}   map_parser (⍵ 1)}
tag_two   ← {{ tag_o tag_w tag_t              ⍵}   map_parser (⍵ 2)}
tag_three ← {{ tag_e tag_e tag_r tag_h tag_t  ⍵}   map_parser (⍵ 3)}
tag_four  ← {{ tag_r tag_u tag_o tag_f        ⍵}   map_parser (⍵ 4)}
tag_five  ← {{ tag_e tag_v tag_i tag_f        ⍵}   map_parser (⍵ 5)}
tag_six   ← {{ tag_x tag_i tag_s              ⍵}   map_parser (⍵ 6)}
tag_seven ← {{ tag_n tag_e tag_v tag_e tag_s  ⍵}   map_parser (⍵ 7)}
tag_eight ← {{ tag_t tag_h tag_g tag_i tag_e  ⍵}   map_parser (⍵ 8)}
tag_nine  ← {{ tag_e tag_n tag_i tag_n        ⍵}   map_parser (⍵ 9)}

⍝ Map le résultat positif d'une fonction de check
⍝ sinon ne fait rien
map ← {
  value ← (⍵)[2]
  res ← ⍺⍺ ⊃⊃⍵
  ⊃res : ⊂(res, value) ⋄ res
}

⍝ Map le résultat d'un parser
map_parser ← {
  data ← 1↑⍵
  value ← 1↓⍵
  (⍺⍺ parser) map (data value)
} 

⍝ Transforme un check en parser
parser ← {
  data ← ⍵[2]
  res ← ⍺⍺ ⍵
  ⊃res : res ⋄ ↑(0 data) 
} 

⍝ Combine un tableau de parser
⍝ Applique tous les parser
⍝ Renvoie le premier qui match
⍝ ou rien
oneof ← { 
  res ← ⊃(⍺⍺ ¨ ⊂⍵)
  ⍝https://stackoverflow.com/questions/77594685/filter-a-list-of-list-based-on-data/77594809#77594809
  (res/⍨1=⊃¨res)
}

⍝ Défini les parser de haut niveau
numbers_letters ← (tag_one, tag_two, tag_three, tag_four, tag_five, tag_six, tag_seven, tag_eight, tag_nine) oneof
numbers_n       ← (tag_1, tag_2, tag_3, tag_4, tag_5, tag_6, tag_7, tag_8, tag_9) oneof
numbers         ← (numbers_n , numbers_letters) oneof


⍝ Parse récursivement la chaîne pour trouver les nombre en chiffre ou en lettres
⍝ les accumule dans un tableau
parse ← {

  ⍝ Paramètre par défaut
  ⍺ ← 0 

  run ← {

    ⍝ Sauvegarde la valeur d'entrée
    prev ← ⍵ 
    ⍝ Applique le parser à l'entrée
    res ← numbers (1 ⍵)

    ⍝ Définit le comportement lorsque le parser a réussi
    good ← {

      ⍝ Récupère les données de retour du parser
      data ← ⊃⍵
      ⍝ Récupère la valeur de mapping de résultat
      to_acc ← ⍺, data[3]

      ⍝ Applique récursivement le parser sur l'entrée décalée d'un caractère vers la droite
      to_acc parse 1↓prev
    }

    ⍝ Vérifie que le parsen a réussi
    ⍝ si oui lance la fonction d'accumulation du résultat parsé
    ⍝ sinon applique le parser sur l'entrée décalée d'un caractère vers la droite
    ⊃⊃res : ⍺ good res ⋄ ⍺ parse 1↓prev


  }

  ⍝ Vérifie qu'il reste des choses à parser
  ⍝ si oui lance la fonction de calcul
  ⍝ si non enlève la première valeur accumulée et retourne le tableau de résultat
  (⍴⍵)>0 : ⍺ run ⍵ ⋄ ⍉(↑(1↓⍺))

}

⍝ Applique le parser créer la réponse de la ligne
get_value ← {
  res ← parse ⍵
  ⊃⌽res+⊃res×10
}


words ← ⊃⎕NGET'/app/data2.txt' 1


⎕ ← +/(get_value ¨ words)




⎕ ← get_value '7ninepnclmdnv7ninevmqoneightpct'
⍝⎕ ← parse 'two45bpple4578'
⍝⎕ ← parse 'threebpple4578'
⍝⎕ ← parse 'four45bpple4578'
⍝⎕ ← parse 'fiveapple4578'
⍝⎕ ← parse 'six45bpple4578'
⍝⎕ ← parse 'sevenbpple4578'
⍝⎕ ← parse 'eight45bpple4578'
⍝⎕ ← parse 'nine45bpple4578'
⍝
⍝⎕ ← parse '1oneapple4578'
⍝⎕ ← parse '2two45bpple4578'
⍝⎕ ← parse '3threebpple4578'
⍝⎕ ← parse '4four45bpple4578'
⍝⎕ ← parse '5fiveapple4578'
⍝⎕ ← parse '6six45bpple4578'
⍝⎕ ← parse '7sevenbpple4578'
⍝⎕ ← parse '8eight45bpple4578'
⍝⎕ ← parse '9nine45bpple4578'

⍝  parser input oneapple4578six => 1 4578 6 => 16
⍝                  ^
⍝ ┌─┬─────────┬───┐
⍝ │1│apple4578│1 3│
⍝ └─┴─────────┴───┘
⍝  parser input  two45bpple4578
⍝ ┌─┬───────────┬───┐
⍝ │1│45bpple4578│2 3│
⍝ └─┴───────────┴───┘
⍝  parser input  threebpple4578
⍝ ┌─┬─────────┬───┐
⍝ │1│bpple4578│3 5│
⍝ └─┴─────────┴───┘
⍝  parser input  four45bpple4578
⍝ ┌─┬───────────┬───┐
⍝ │1│45bpple4578│4 4│
⍝ └─┴───────────┴───┘
⍝  parser input  fiveapple4578
⍝ ┌─┬─────────┬───┐
⍝ │1│apple4578│5 4│
⍝ └─┴─────────┴───┘
⍝  parser input  six45bpple4578
⍝ ┌─┬───────────┬───┐
⍝ │1│45bpple4578│6 3│
⍝ └─┴───────────┴───┘
⍝  parser input  sevenbpple4578
⍝ ┌─┬─────────┬───┐
⍝ │1│bpple4578│7 5│
⍝ └─┴─────────┴───┘
⍝  parser input  eight45bpple4578
⍝ ┌─┬───────────┬───┐
⍝ │1│45bpple4578│8 5│
⍝ └─┴───────────┴───┘
⍝  parser input  nine45bpple4578
⍝ ┌─┬───────────┬───┐
⍝ │1│45bpple4578│9 4│
⍝ └─┴───────────┴───┘
⍝  parser input  1oneapple4578
⍝ ┌─┬────────────┬───┐
⍝ │1│oneapple4578│1 1│
⍝ └─┴────────────┴───┘
⍝  parser input  2two45bpple4578
⍝ ┌─┬──────────────┬───┐
⍝ │1│two45bpple4578│2 1│
⍝ └─┴──────────────┴───┘
⍝  parser input  3threebpple4578
⍝ ┌─┬──────────────┬───┐
⍝ │1│threebpple4578│3 1│
⍝ └─┴──────────────┴───┘
⍝  parser input  4four45bpple4578
⍝ ┌─┬───────────────┬───┐
⍝ │1│four45bpple4578│4 1│
⍝ └─┴───────────────┴───┘
⍝  parser input  5fiveapple4578
⍝ ┌─┬─────────────┬───┐
⍝ │1│fiveapple4578│5 1│
⍝ └─┴─────────────┴───┘
⍝  parser input  6six45bpple4578
⍝ ┌─┬──────────────┬───┐
⍝ │1│six45bpple4578│6 1│
⍝ └─┴──────────────┴───┘
⍝  parser input  7sevenbpple4578
⍝ ┌─┬──────────────┬───┐
⍝ │1│sevenbpple4578│7 1│
⍝ └─┴──────────────┴───┘
⍝  parser input  8eight45bpple4578
⍝ ┌─┬────────────────┬───┐
⍝ │1│eight45bpple4578│8 1│
⍝ └─┴────────────────┴───┘
⍝  parser input  9nine45bpple4578
⍝ ┌─┬───────────────┬───┐
⍝ │1│nine45bpple4578│9 1│
⍝ └─┴───────────────┴───┘